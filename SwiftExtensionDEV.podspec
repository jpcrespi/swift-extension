Pod::Spec.new do |s|

  s.name                  = "SwiftExtension"
  s.version               = "2.0.8"
  s.summary               = "Swift Extension"
  s.description           = <<-DESC
                            Swift Extension by Juan Pablo Crespi
                           DESC
  s.homepage              = "https://gitlab.com/jpcrespi/swift-extension"
  s.license               = { :type => "Commercial", :text => "Copyright 2017" }
  s.author                = "Juan Pablo Crespi"

  s.platform        	    = :ios, "10.0"
  s.source          	    = { "path" => "." }
  s.requires_arc    	    = true
  s.default_subspec       = 'Core'
  s.swift_versions        = ['5.5']

  s.subspec 'Core' do |core|
    core.source_files = 'Sources/Core/**/*.*'
  end

  s.subspec 'Alamofire' do |alamofire|
    alamofire.source_files = 'Sources/Alamofire/**/*.*'
    alamofire.dependency 'SwiftExtension/Core'
    alamofire.dependency 'Alamofire', '> 5'
  end

  s.subspec 'Kingfisher' do |kingfisher|
    kingfisher.source_files = 'Sources/Kingfisher/**/*.*'
    kingfisher.dependency 'SwiftExtension/Core'
    kingfisher.dependency 'Kingfisher', '> 6'
  end
      
  s.subspec 'SwiftyJSON' do |swiftyjson|
    swiftyjson.source_files = 'Sources/SwiftyJSON/**/*.*'
    swiftyjson.dependency 'SwiftExtension/Core'
    swiftyjson.dependency 'SwiftyJSON'
  end

  s.subspec 'Realm' do |realm|
    realm.source_files = 'Sources/Realm/**/*.*'
    realm.dependency 'SwiftExtension/Core'
    realm.dependency 'RealmSwift'
  end

  s.subspec 'RemoteConfig' do |remoteConfig|
    remoteConfig.source_files = 'Sources/RemoteConfig/**/*.*'
    remoteConfig.dependency 'SwiftExtension/Core'
    remoteConfig.dependency 'Firebase/RemoteConfig'
  end

  s.subspec 'Messaging' do |messaging|
    messaging.source_files = 'Sources/Messaging/**/*.*'
    messaging.dependency 'SwiftExtension/Core'
    messaging.dependency 'Firebase/Messaging'
  end
end
