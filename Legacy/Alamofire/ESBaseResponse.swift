//
//  ESBaseResponse.swift
//  mbtb
//
//  Created by Juan Pablo Crespi on 1/27/17.
//  Copyright © 2017 Juan Pablo Crespi. All rights reserved.
//

import UIKit

open class LOG: NSObject {
    open class func SUCCESS(_ message: String) {
        NSLog("[<<<<<<SERVICE>>>>>>][SUCCESS] -> %@", message)
    }
    
    open class func ERROR(_ message: String) {
        NSLog("[<<<<<<SERVICE>>>>>>][ERROR] -> %@", message)
    }
    
    open class func RETRY(_ count: Int, _ message: String) {
        NSLog("[<<<<<<SERVICE>>>>>>][RETRY \(count)] -> %@", message)
    }
}

open class ESBaseResponse: NSObject {
    
    open func serviceResult(_ serviceResult : AnyObject?, responseCode: Int) -> Self {
        return self
    }
    
    open func serviceError(_ serviceError: NSError?, responseCode: Int = 0) -> Self {
        return self
    }
    
    open func database() -> Self {
        return self
    }
    
    open func error(_ messange: String) -> Self {
        return self
    }
    
    open func parseData(_ object: Any, completion: @escaping (() -> Void)) {
        completion()
    }
    
    open func build(_ completion: @escaping (() -> Void)) {
        completion()
    }
    
    open func logService(_ name: String) {

    }
    
    open func hashKey() -> String? {
        return nil
    }
    
    open func hashValue(_ object: Any) -> Bool {
        return true
    }
}
