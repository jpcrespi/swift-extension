//
//  ESError.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 1/27/17.
//  Copyright © 2017 Juan Pablo Crespi. All rights reserved.
//

import UIKit

open class ESBaseError: NSObject {
    
    open var id: String = ""
    open var code: String = ""
    open var text: String = ""
    open var message: String = ""
    
}
