//
//  ESRequest.swift
//  pizzahut
//
//  Created by Juan Pablo Crespi on 21/12/15.
//  Copyright © 2015 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import Alamofire

public var stagingUser: String? = nil
public var stagingPassword: String? = nil
public var defaultTimeout: TimeInterval = 60
public var defaultRetry: Int = 0

open class ESRequest: ESBaseRequest<ESBaseResponse> { }

open class ESBaseRequest<T: ESBaseResponse>: NSObject {
    
    public enum ResponseType {
        case JSON, String
    }
    
    open var name: String = ""
    open var method: HTTPMethod
    open var encoding: ParameterEncoding
    open var path : URLConvertible
    open var response : T
    open var header: [String: String] = SessionManager.defaultHTTPHeaders
    open var body: [String: AnyObject]?
    open var images: [String: UIImage]?
    open var timeout: TimeInterval
    open var retry: Int
    open var responseType: ResponseType
    
    open var retryCount: Int = 0

    public init (method : HTTPMethod,
          encoding: ParameterEncoding,
          path : URLConvertible,
          response : T,
          header: [String : String]? = nil,
          body: [String : AnyObject]? = nil,
          images : [String : UIImage]? = nil,
          timeout: TimeInterval? = nil,
          retry: Int? = nil,
          responseType: ResponseType = .JSON) {
        
        self.method = method
        self.encoding = encoding
        self.path = path
        self.response = response
        self.header += header ?? [:]
        self.body = body
        self.images = images
        self.timeout = timeout ?? defaultTimeout
        self.retry = retry ?? defaultRetry
        self.responseType = responseType
        
        super.init()

        self.manager.delegate.taskWillPerformHTTPRedirection = nil
        self.manager.retrier = self
        self.manager.adapter = self
    }
    
    lazy var manager: Alamofire.SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = self.timeout
        return Alamofire.SessionManager(configuration: configuration)
    }()
}
    
extension ESBaseRequest: RequestAdapter {
        
    open func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        return urlRequest
    }
}
    
extension ESBaseRequest: RequestRetrier {
        
    open func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        let errorCodes = [-1009, -1001, 408]
        if errorCodes.contains((error as NSError).code) && retryCount < retry {
            retryCount += 1
            LOG.RETRY(retryCount, request.request?.url?.absoluteString ?? "")
            completion(true, 0)
        }
        else {
            completion(false, 0)
        }
    }
}
    
extension ESBaseRequest {
    
    open func performRequest(_ resultHandler: @escaping ((T) -> ())) -> Alamofire.Request {
        let request = manager.request(path, method: method, parameters: body, encoding: encoding, headers: header)
        
        setAuthenticate(request)
        executeRequest(request: request, resultHandler: resultHandler)
        return request
    }
    
    open func uploadRequest(_ resultHandler:@escaping ((T) -> Void)) -> Void {
        
        manager.upload(multipartFormData: { multipartFormData in
            if let images = self.images {
                for (name, image) in images {
                    if let imageData = image.jpegData(compressionQuality: 0.5) {
                        multipartFormData.append(imageData, withName: name, fileName: "\(name).jpeg", mimeType: "image/jpeg")
                    }
                }
            }
            if let body = self.body as? [String: String] {
                for (key, value) in body {
                    multipartFormData.append((value.data(using: .utf8))!, withName: key)
                }
            }
        }, to: path,
           method: method,
           headers: header,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let request, _, _):
                self.executeRequest(request: request, resultHandler: resultHandler)
            case .failure(let error):
                resultHandler(self.response.serviceError(error as NSError?))
            }
        })
    }
    
    open func setAuthenticate(_ request: Alamofire.Request) {
        if let user = stagingUser, let password = stagingPassword {
            request.authenticate(user: user, password: password)
            URLCache.shared.removeAllCachedResponses()
        }
    }
    
    func executeRequest(request: DataRequest, resultHandler: @escaping ((T) -> ())) {
        switch responseType {
        case .JSON:
            request.responseJSON {
                self.executeResponse(response: $0, resultHandler: resultHandler)
            }
        case .String:
            request.responseString {
                self.executeResponse(response: $0, resultHandler: resultHandler)
            }
        }
    }
    
    func executeResponse<V>(response: DataResponse<V>, resultHandler: @escaping ((T) -> ())) {
        let responseCode = response.response?.statusCode ?? 0
        switch response.result {
        case .success(let data):
            resultHandler(self.response.serviceResult(data as AnyObject?, responseCode: responseCode))
        case .failure(let error):
            resultHandler(self.response.serviceError(error as NSError?, responseCode: responseCode))
        }
    }
}
