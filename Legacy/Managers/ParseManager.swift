//
//  ParseManager.swift
//  mbtb
//
//  Created by Juan Pablo Crespi on 1/24/17.
//  Copyright © 2017 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

public protocol ParseDelegate {
    func parse(json: JSON)
}

open class Parse<T: NSManagedObject>: NSObject {
    
    open class func array(
        jsons: [JSON],
        strength: DataStrength,
        uniqueKey: String? = nil,
        completion: ((_ objects: [T]) -> ())?) {
        
        var array: [T] = []
        for json in jsons {
            object(
                json: json,
                strength: strength,
                uniqueKey: uniqueKey,
                completion: { object in
                    array.append(object)
            })
        }
        
        completion?(array)
    }
    
    open class func object(
        json: JSON,
        object: T? = nil,
        strength: DataStrength,
        uniqueKey: String? = nil,
        completion: ((_ object: T) -> ())?) {
        
        var fetchObject: T?
        if let key = uniqueKey {
            fetchObject =  T.fetchObjects(predicate: "\(key) == \(json["\(key)"])".predicate).first
        }
        
        let nonOptional = object ?? fetchObject ?? T.create(strength)

        if let delegate = nonOptional as? ParseDelegate {
            delegate.parse(json: json)
        }
        else {
            fatalError("\(T.className) does not conform the protocol ParseDelegate")
        }
        
        completion?(nonOptional)
    }
}

public extension JSON {
    
    //Non-optional [String : Any]
    var dictionaryObjectValue: [String : Any] {
        return self.dictionaryObject ?? [:]
    }
    
}
