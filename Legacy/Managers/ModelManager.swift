//
//  ModelManager.swift
//  DataLayer
//
//  Created by Juan Pablo Crespi on 5/16/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit
import CoreData

public enum DataStrength: Int {
    case temporary
    case permanent
    case background
}

open class ModelManager: NSObject {
    
    open class var sharedInstance : ModelManager {
        struct Static {
            static let sharedManager = ModelManager()
        }
        return Static.sharedManager
    }
    
    public static var dataModelName = ""
    public static var bundleIdentifier = ""
    public static var pathComponent = ""
    public static var withExtension = ""
    
    //MARK: - Core Data stack
    
    override init() {
        super.init()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(contextDidSaveContext(_:)),
            name: NSNotification.Name.NSManagedObjectContextDidSave, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    fileprivate lazy var applicationDocumentsDirectory: URL = {
        let urls = Foundation.FileManager.default.urls(
            for: .documentDirectory,
            in: .userDomainMask)
        
        return urls[urls.count-1]
    }()
    
    fileprivate lazy var managedObjectModel: NSManagedObjectModel = {
        if let customKitBundle = Bundle(identifier: ModelManager.bundleIdentifier) {
            if let modelURL = customKitBundle.url(forResource: ModelManager.dataModelName, withExtension: ModelManager.withExtension) {
                if let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) {
                    return managedObjectModel
                }
            }
        }
        fatalError("bundleIdentifier: \(ModelManager.bundleIdentifier), dataModelName: \(ModelManager.dataModelName)")
    }()
    
    fileprivate lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        let options = [NSMigratePersistentStoresAutomaticallyOption : true,
                       NSInferMappingModelAutomaticallyOption : true]
        
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("\(ModelManager.pathComponent).sqlite")
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator!.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: options)
        } catch var error1 as NSError {
            error = error1
            coordinator = nil
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            NSLog("Unresolved error \(error?.localizedDescription ?? ""), \(error?.userInfo ?? [:])")
            abort()
        } catch {
            fatalError()
        }
        
        return coordinator  
    }()
    
    fileprivate lazy var permanentContext: NSManagedObjectContext? = {
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    fileprivate lazy var volatileContext: NSManagedObjectContext? = {
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    fileprivate lazy var backgroundContext: NSManagedObjectContext? = {
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    fileprivate func saveContext(_ context: NSManagedObjectContext?) {
        var error: NSError? = nil
        if let moc = context , moc.hasChanges {
            do {
                try moc.save()
            } catch let error1 as NSError {
                error = error1
                NSLog("Unresolved error \(error?.localizedDescription ?? ""), \(error?.userInfo ?? [:])")
                abort()
            }
        }
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    
    open func context(_ strength: DataStrength) -> NSManagedObjectContext {
        switch strength {
        case .permanent:
            return ModelManager.sharedInstance.permanentContext!
        case .temporary:
            return ModelManager.sharedInstance.volatileContext!
        case .background:
            return ModelManager.sharedInstance.backgroundContext!
        }
    }
    
    open func save(_ strength: DataStrength) {
        switch strength {
        case .permanent:
            saveContext(self.permanentContext)
        case .temporary:
            NSLog("******** Temporary Not Save")
        case .background:
            saveContext(self.backgroundContext)
        }
    }
    
///////////////////////////////////////////////////////////////////////////////////////////////////
    
    @objc func contextDidSaveContext(_ notification: Foundation.Notification) { }
    
    let backgroundThreadActive = false
    
    open func backgroundThread(_ strength: DataStrength,
                               background: ((_ backgroundContext: NSManagedObjectContext) -> Void)? = nil,
                               completion: (() -> Void)? = nil) {
        
        if backgroundThreadActive == false {
            
            if let backgroundBlock = background {
                backgroundBlock(ModelManager.sharedInstance.context(strength))
            }
            if strength == .permanent {
                ModelManager.sharedInstance.save(.permanent)
            }
            if let completionBlock = completion {
                completionBlock()
            }
        }
        else {
            if #available(iOS 8.0, *) {
                DispatchQueue.global(qos: .userInitiated).async(execute: {
                    let backgroundContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
                    backgroundContext.persistentStoreCoordinator = self.persistentStoreCoordinator
                    if let backgroundBlock = background {
                        backgroundBlock(backgroundContext)
                    }
                    if strength == .permanent {
                        ModelManager.sharedInstance.saveContext(backgroundContext)
                    }
                    
                    DispatchQueue.main.async(execute: {
                        if let completionBlock = completion {
                            completionBlock()
                        }
                    })
                })
            } else {
                // Fallback on earlier versions
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////

extension NSManagedObject {
    
    fileprivate class func context(_ strength: DataStrength) -> NSManagedObjectContext {
        return ModelManager.sharedInstance.context(strength)
    }
    
    open class func entityFetchRequest<T: NSManagedObject>() -> NSFetchRequest<T> {
        return NSFetchRequest<T>(entityName: String(describing: self.className))
    }
    
    open class func entityDescription(_ context: NSManagedObjectContext) -> NSEntityDescription {
        return NSEntityDescription.entity(forEntityName: String(describing: self.className), in: context)!
    }

    //MARK - INIT
    open class func create<T: NSManagedObject>(_ strength: DataStrength) -> T {
        return create(context(strength))
    }
    
    open class func create<T: NSManagedObject>(_ context: NSManagedObjectContext) -> T {
        return NSManagedObject(entity: entityDescription(context), insertInto: context) as! T
    }
    
    //MARK - GET
    open class func fetchObjects<T: NSManagedObject>(
        predicate: NSPredicate? = nil,
        context: NSManagedObjectContext? = nil,
        order: NSSortDescriptor? = nil) -> [T] {
        
        let request: NSFetchRequest<T> = entityFetchRequest()
        
        if let requestPredicate = predicate {
            request.predicate = requestPredicate
        }
        
        if let sortDescriptor = order {
            request.sortDescriptors = [sortDescriptor]
        }
        
        return fetchRequest(request: request, context: context)
    }
    
    open class func fetchTemplate<T: NSManagedObject>(
        template: String,
        context: NSManagedObjectContext? = nil,
        predicate: NSPredicate? = nil,
        order: NSSortDescriptor? = nil) -> [T] {

        let model = ModelManager.sharedInstance.managedObjectModel
        let request: NSFetchRequest<T>? = model.fetchRequestTemplate(forName: template)?.copy() as? NSFetchRequest<T>
    
        if let templatePredicate = request?.predicate, let requestPredicate = predicate {
            request?.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [templatePredicate, requestPredicate])
        }
    
        if let sortDescriptor = order {
            request?.sortDescriptors = [sortDescriptor]
        }
        
        return fetchRequest(request: request, context: context)
    }
    
    open class func singleObject<T: NSManagedObject>() -> T? {
        let request: NSFetchRequest<T> = entityFetchRequest()
        request.fetchLimit = 1
        
        return fetchRequest(request: request).first
    }
    
    fileprivate class func fetchRequest<T: NSManagedObject>(
        request: NSFetchRequest<T>?,
        context: NSManagedObjectContext? = nil)  -> [T]  {
        do {
            if let fetchRequest = request {
                return try (context ?? self.context(.permanent)).fetch(fetchRequest)
            }
            else {
                return []
            }
        }
        catch {
            return []
        }
    }
    
    fileprivate class func executeRequest(
        request: NSPersistentStoreRequest?,
        context: NSManagedObjectContext? = nil)  {
        do {
            if let storeRequest = request {
                try (context ?? self.context(.permanent)).execute(storeRequest)
            }
        }
        catch { }
    }
    
    //MARK - REMOVE
    open class func removeObjects(predicate: NSPredicate? = nil, context: NSManagedObjectContext? = nil) {
        entityRemoveObjects(objects: fetchObjects(), context: context)

        //let request = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: self))
        //request.predicate = predicate
        //executeRequest(request: NSBatchDeleteRequest(fetchRequest: request))
    }
    
    open class func removeObject(_ object: NSManagedObject?, context: NSManagedObjectContext? = nil) {
        entityRemoveObjects(objects: [object], context: context)
    }
    
    open class func removeObjects(_ objects: [NSManagedObject]?, context: NSManagedObjectContext? = nil) {
        entityRemoveObjects(objects: objects, context: context)
    }
    
    fileprivate class func entityRemoveObjects(objects: [NSManagedObject?]?, context: NSManagedObjectContext? = nil) {
        for object in objects ?? [] {
            if let obj = object {
                (context ?? self.context(.permanent)).delete(obj)
            }
        }
    }
    
    //MARK - CLONE
    open func clone<T: NSManagedObject>() -> T {
        return cloneInContext(managedObjectContext!) as! T
    }
    
    open func clone<T: NSManagedObject>(_ strength: DataStrength) -> T {
        return cloneInContext(NSManagedObject.context(strength)) as! T
    }
    
    open func clone<T: NSManagedObject>(_ context: NSManagedObjectContext) -> T {
        return cloneInContext(context) as! T
    }
    
    fileprivate func cloneInContext(
        _ context: NSManagedObjectContext,
        alreadyCopied: NSMutableDictionary = NSMutableDictionary(),
        namesOfEntitiesToExclude: NSMutableArray = NSMutableArray(capacity: 1),
        firstPass: Bool = true) -> NSManagedObject? {
        
        var firstPass = firstPass
        let entityName = entity.name!
        
        if namesOfEntitiesToExclude.contains(entityName) {
            return nil
        }
        
        if let cloned = alreadyCopied.object(forKey: objectID) as? NSManagedObject {
            return cloned
        }
        else {
            let cloned = NSEntityDescription.insertNewObject(
                forEntityName: entityName, into: context)
            
            let attributes = NSEntityDescription.entity(
                forEntityName: entityName, in: context)?.attributesByName
            
            alreadyCopied.setObject(cloned, forKey: objectID)
            
            for attribute in attributes! {
                cloned.setValue(value(forKey: attribute.0), forKey: attribute.0)
            }
            
            if firstPass == true {
                namesOfEntitiesToExclude.add(entityName)
                firstPass = false
            }
            
            if let relationships = NSEntityDescription.entity(
                forEntityName: entityName,
                in: context)?.relationshipsByName {
                
                for relationshipName in relationships.keys {
                    
                    if let relationship = relationships[relationshipName] {
                        
                        if relationship.isToMany {
                            if relationship.isOrdered {
                                
                                let sourceSet = mutableOrderedSetValue(forKey: relationship.name)
                                let clonedSet = cloned.mutableOrderedSetValue(forKey: relationship.name)
                                
                                let enumerator = sourceSet.objectEnumerator()
                                
                                while let relatedObject = enumerator.nextObject() as? NSManagedObject {
                                    
                                    let clonedRelatedObject = relatedObject.cloneInContext(
                                        context,
                                        alreadyCopied: alreadyCopied,
                                        namesOfEntitiesToExclude: namesOfEntitiesToExclude,
                                        firstPass: firstPass)
                                    
                                    if let clonedRelatedObject = clonedRelatedObject {
                                        clonedSet.add(clonedRelatedObject)
                                        clonedSet.add(clonedRelatedObject)
                                    }
                                }
                            }
                            else {
                                let sourceSet = mutableSetValue(forKey: relationship.name)
                                let clonedSet = cloned.mutableSetValue(forKey: relationship.name)
                                
                                let enumerator = sourceSet.objectEnumerator()
                                
                                while let relatedObject = enumerator.nextObject() as? NSManagedObject {
                                    
                                    let clonedRelatedObject = relatedObject.cloneInContext(
                                        context,
                                        alreadyCopied: alreadyCopied,
                                        namesOfEntitiesToExclude:
                                        namesOfEntitiesToExclude,
                                        firstPass: firstPass)
                                    
                                    if let clonedRelatedObject = clonedRelatedObject {
                                        clonedSet.add(clonedRelatedObject)
                                    }
                                }
                            }
                        }
                        else {
                            if let relatedObject = value(forKey: relationship.name) as? NSManagedObject {
                                
                                let clonedRelatedObject = relatedObject.cloneInContext(
                                    context, alreadyCopied: alreadyCopied,
                                    namesOfEntitiesToExclude: namesOfEntitiesToExclude,
                                    firstPass: firstPass)
                                
                                cloned.setValue(clonedRelatedObject, forKey: relationship.name)
                            }
                        }
                    }
                }
            }
            return cloned
        }
    }
}



