//
//  LocationManager.swift
//  pizzahut
//
//  Created by Juan Pablo Crespi on 8/1/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

@objc public protocol LocationManagerAuthorizationDelegate {
    @objc optional func locationManagerAuthorized(status: CLAuthorizationStatus)
}

@objc public protocol LocationManagerDelegate {
    @objc optional func onLocation(location: CLLocation)
}

open class LocationManager: NSObject {
    
    fileprivate var manager = CLLocationManager()
    fileprivate var delegate : LocationManagerAuthorizationDelegate?
    fileprivate var authorizedType: CLAuthorizationStatus?
    
    open class var sharedInstance : LocationManager {
        struct Static {
            static let sharedManager = LocationManager()
        }
        return Static.sharedManager
    }
    
    open var location: CLLocation?
    open var locationDelegate: LocationManagerDelegate?
    
    override init() {
        super.init()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyHundredMeters
    }
    
    open func getLocation(delegate: LocationManagerDelegate) {
        locationDelegate = delegate
    }
    
    open var authorized: Bool {
        return CLLocationManager.authorizationStatus() == .authorizedAlways ||
                CLLocationManager.authorizationStatus() == .authorizedWhenInUse
    }
    
    open var authorizedStatus: CLAuthorizationStatus {
        return CLLocationManager.authorizationStatus()
    }
    
    open func requestAuthorization(authorization status: CLAuthorizationStatus, delegate controller: LocationManagerAuthorizationDelegate) {
        delegate = controller
        authorizedType = status
        
        #if arch(i386) || arch(x86_64)
            location = CLLocation(latitude: -32.951459, longitude: -60.648188)
            delegate?.locationManagerAuthorized?(status: CLLocationManager.authorizationStatus())
        #else
            if CLLocationManager.authorizationStatus() == .notDetermined {
                switch status {
                case .authorizedAlways:
                    if manager.responds(to: #selector(CLLocationManager.requestAlwaysAuthorization)) {
                        manager.requestAlwaysAuthorization()
                    } else if manager.responds(to: #selector(CLLocationManager.requestLocation)) {
                        manager.requestLocation()
                    } else {
                        delegate?.locationManagerAuthorized?(status: .notDetermined)
                    }
                case .authorizedWhenInUse:
                    if manager.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)) {
                        manager.requestWhenInUseAuthorization()
                    } else if manager.responds(to: #selector(CLLocationManager.requestLocation)) {
                        manager.requestLocation()
                    } else {
                        delegate?.locationManagerAuthorized?(status: .notDetermined)
                    }
                case .denied, .restricted, .notDetermined:
                    delegate?.locationManagerAuthorized?(status: status)
                @unknown default:
                    fatalError()
                }
            } else {
                delegate?.locationManagerAuthorized?(status: CLLocationManager.authorizationStatus())
                if authorized { startUpdatingLocation() }
            }
        #endif
    }
    
    open func startUpdatingLocation() {
        #if arch(i386) || arch(x86_64)
            NSLog("[<<<<<<LOCATION>>>>>] START_SIMULATOR_NOT_SUPPORTED")
        #else
            if authorized {
                #if os(iOS)
                    manager.startUpdatingLocation()
                #elseif os(watchOS)
                    manager.requestLocation()
                #endif
                
                NSLog("[<<<<<<LOCATION>>>>>] startUpdatingLocation")
            }
        #endif
    }
    
    open func stopUpdatingLocation() {
        #if arch(i386) || arch(x86_64)
            NSLog("[<<<<<<LOCATION>>>>>] STOP_SIMULATOR_NOT_SUPPORTED")
        #else
            if authorized {
                manager.stopUpdatingLocation()
                NSLog("[<<<<<<LOCATION>>>>>] stopUpdatingLocation")
            }
        #endif
    }
}

extension LocationManager: CLLocationManagerDelegate {
    
    open func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .authorizedAlways:
            NSLog("[<<<<<<LOCATION>>>>>] AUTHORIZED_ALWAYS")
        case .authorizedWhenInUse:
            NSLog("[<<<<<<LOCATION>>>>>] AUTHORIZED_WHEN_IN_USE")
        case .denied:
            NSLog("[<<<<<<LOCATION>>>>>] AUTHORIZED_DENIED")
        case .restricted:
            NSLog("[<<<<<<LOCATION>>>>>] AUTHORIZED_RESTRICTED")
        case .notDetermined:
            NSLog("[<<<<<<LOCATION>>>>>] AUTHORIZED_NOT_DETERMINED")
        @unknown default:
            fatalError()
        }
        
        if status != .notDetermined {
            delegate?.locationManagerAuthorized?(status: status)
            if authorized { startUpdatingLocation() }
        }
    }
    
    open func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        location = locations.first
        if let location = locations.first {
            locationDelegate?.onLocation?(location: location)
            locationDelegate = nil
        }
    }
    
    open func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        NSLog("[<<<<<<LOCATION>>>>>] ERROR \(error.localizedDescription)");
    }
}

extension LocationManager {
    
    open func applicationDidEnterBackground() {
        stopUpdatingLocation()
    }
    
    open func applicationWillEnterForeground() {
        startUpdatingLocation()
    }
}
