//
//  FileManager.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 7/25/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

open class FileManager: NSObject {
    
    class open func jsonFromFile(_ fileLocation: String, bundle: Bundle = Bundle.main) -> AnyObject? {
        
        let file = fileLocation as NSString
        if let path = bundle.path(
            forResource: file.deletingPathExtension,
            ofType: file.pathExtension) {
            do {
                let data = try Data(
                    contentsOf: URL(fileURLWithPath: path),
                    options: .mappedIfSafe)
                return try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as AnyObject?
            } catch _ as NSError {
                return nil
            }
        }
        else {
            return nil
        }
    }
    
    class open func stringFromFile(_ fileLocation: String, encoding: String.Encoding = .utf8, bundle: Bundle = Bundle.main) -> String? {
        
        let file = fileLocation as NSString
        if let path = bundle.path(
            forResource: file.deletingPathExtension,
            ofType: file.pathExtension) {
            do {
                return try NSString(contentsOfFile: path, encoding: encoding.rawValue) as String
            } catch _ as NSError {
                return nil
            }
        } else {
            return nil
        }
    }
}
