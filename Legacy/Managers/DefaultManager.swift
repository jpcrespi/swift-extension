//
//  DefaultManager.swift
//  DataLayer
//
//  Created by Juan Pablo Crespi on 5/17/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

open class DefaultManager: NSObject {
    
    public static var userDefaults: UserDefaults = .standard
    
    open class func getBool(_ key: String) -> Bool {
        return userDefaults.bool(forKey: key)
    }
    
    open class func setBool(_ value: Bool, key: String) {
        userDefaults.set(value, forKey: key)
    }
    
    open class func getInt(_ key: String) -> Int {
        return userDefaults.integer(forKey: key)
    }
    
    open class func setInt(_ value: Int, key: String) {
        userDefaults.set(value, forKey: key)
    }
    
    open class func getString(_ key: String) -> String? {
        return userDefaults.string(forKey: key)
    }
    
    open class func setString(_ value: String?, key: String) {
        userDefaults.set(value, forKey: key)
    }
    
    open class func getObject(_ key: String) -> AnyObject? {
        return userDefaults.object(forKey: key) as AnyObject?
    }
    
    open class func setObject(_ value: AnyObject?, key: String) {
        userDefaults.set(value, forKey: key)
    }
    
    open class func existKey(_ key: String) -> Bool {
        return userDefaults.dictionaryRepresentation().keys.contains(key)
    }
}
