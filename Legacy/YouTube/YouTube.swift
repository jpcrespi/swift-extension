//
//  YouTube.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 8/9/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import UIKit
import AVFoundation

public enum PlayerCallback: String {
    case onReady
    case onStateChange
    case onPlaybackQualityChange
    case onPlayerError
}

public enum PlayerState: String {
    case YTPlayerStateUnstarted
    case YTPlayerStateEnded
    case YTPlayerStatePlaying
    case YTPlayerStatePaused
    case YTPlayerStateBuffering
    case YTPlayerStateCued
    case YTPlayerStateUnknown
}

public enum PlayerQuality: String {
    case kYTPlaybackQualitySmallQuality
    case kYTPlaybackQualityMediumQuality
    case kYTPlaybackQualityLargeQuality
    case YTPlaybackQualityHD720Quality
    case YTPlaybackQualityHD1080Quality
    case YTPlaybackQualityHighResQuality
    case YTPlaybackQualityAutoQuality
    case YTPlaybackQualityDefaultQuality
    case YTPlaybackQualityUnknownQuality
}

public enum PlayerError: String {
    case YTPlayerErrorInvalidParamError
    case YTPlayerErrorHTML5Error
    case YTPlayerErrorVideoNotFoundError
    case YTPlayerErrorNotEmbeddableError
    case YTPlayerErrorCannotFindVideoError
    case YTPlayerErrorSameAsNotEmbeddableError    
}

public protocol YouTubeDelegate {
    func didStartLoad(_ webView: UIWebView)
    func didFinishLoad(_ webView: UIWebView)
    func didFailLoad(_ webView: UIWebView, withError error: NSError?)
    func onReady(_ webView: UIWebView)
    func onStateChange(_ webView: UIWebView, state: PlayerState)
    func onPlayerError(_ webView: UIWebView, error: PlayerError)
    func onPlaybackQualityChange(_ webView: UIWebView, quality: PlayerQuality)
}

open class YouTube: NSObject {
    
    open var webView: UIWebView
    open var urlString: String
    open var delegate: YouTubeDelegate
    
    public init(webView: UIWebView, urlString: String, delegate: YouTubeDelegate) {
        self.webView = webView
        self.urlString = urlString
        self.delegate = delegate
        
        super.init()
        
        webView.delegate = self
        webView.scrollView.bounces = false
        webView.scrollView.isScrollEnabled = false
        
        if let id = extractYoutubeIdFromLink(urlString) {
            webView.loadHTMLString(embedYouTube(id), baseURL: nil)
        }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print(error)
        }
    }
    
    open func embedYouTube(_ videoID: String) -> String {
        if let  html = stringFromFile("YouTube.html") {
            return html.replacingOccurrences(
                of: "VIDEOID", with: videoID).replacingOccurrences(
                    of: "PLAYSINLINE", with: "0")
        }
        
        return ""
    }
    
    open func extractYoutubeIdFromLink(_ link: String) -> String? {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        guard let regExp = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive) else {
            return nil
        }
        let nsLink = link as NSString
        let options = NSRegularExpression.MatchingOptions(rawValue: 0)
        let range = NSRange(location: 0,length: nsLink.length)
        let matches = regExp.matches(in: link as String, options:options, range:range)
        if let firstMatch = matches.first {
            return nsLink.substring(with: firstMatch.range)
        }
        
        return nil
    }
    
    open func stringFromFile(_ fileLocation: String) -> String? {
        let file = fileLocation as NSString
        if let path = Bundle(for: YouTube.self).path(forResource: file.deletingPathExtension, ofType: file.pathExtension) {
            do {
                return try NSString(contentsOfFile: path, encoding: String.Encoding.utf8.rawValue) as String
            } catch _ as NSError {
                return nil
            }
        } else {
            return nil
        }
    }
    
    open func playVideo() {
        webView.stringByEvaluatingJavaScript(from: "playVideo();")
    }
    
    open func pauseVideo() {
        webView.stringByEvaluatingJavaScript(from: "pauseVideo();")
    }
    
    open func stopVideo() {
        webView.stringByEvaluatingJavaScript(from: "stopVideo();")
    }
}

extension YouTube: UIWebViewDelegate {
    
    open func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        
        if let url = request.url , url.scheme == "youtubeplayer" {
            if let action = url.host {
                if let query = url.query , query.count > 0  {
                    let data = query.components(separatedBy: "=")[1]
                    switch action {
                    case PlayerCallback.onReady.rawValue:
                        delegate.onReady(webView)
                    case PlayerCallback.onStateChange.rawValue:
                        switch data {
                        case PlayerState.YTPlayerStateBuffering.rawValue:
                            delegate.onStateChange(webView, state: .YTPlayerStateBuffering)
                        case PlayerState.YTPlayerStateCued.rawValue:
                            delegate.onStateChange(webView, state: .YTPlayerStateCued)
                        case PlayerState.YTPlayerStateEnded.rawValue:
                            delegate.onStateChange(webView, state: .YTPlayerStateEnded)
                        case PlayerState.YTPlayerStatePaused.rawValue:
                            delegate.onStateChange(webView, state: .YTPlayerStatePaused)
                        case PlayerState.YTPlayerStatePlaying.rawValue:
                            delegate.onStateChange(webView, state: .YTPlayerStatePlaying)
                        case PlayerState.YTPlayerStateUnknown.rawValue:
                            delegate.onStateChange(webView, state: .YTPlayerStateUnknown)
                        case PlayerState.YTPlayerStateUnstarted.rawValue:
                            delegate.onStateChange(webView, state: .YTPlayerStateUnstarted)
                        default:
                            break
                        }
                    case PlayerCallback.onPlaybackQualityChange.rawValue:
                        switch data {
                        case PlayerQuality.YTPlaybackQualityAutoQuality.rawValue:
                            delegate.onPlaybackQualityChange(webView, quality: .YTPlaybackQualityAutoQuality)
                        case PlayerQuality.YTPlaybackQualityHD720Quality.rawValue:
                            delegate.onPlaybackQualityChange(webView, quality: .YTPlaybackQualityHD720Quality)
                        case PlayerQuality.YTPlaybackQualityHD1080Quality.rawValue:
                            delegate.onPlaybackQualityChange(webView, quality: .YTPlaybackQualityHD1080Quality)
                        case PlayerQuality.YTPlaybackQualityDefaultQuality.rawValue:
                            delegate.onPlaybackQualityChange(webView, quality: .YTPlaybackQualityDefaultQuality)
                        case PlayerQuality.YTPlaybackQualityHighResQuality.rawValue:
                            delegate.onPlaybackQualityChange(webView, quality: .YTPlaybackQualityHighResQuality)
                        case PlayerQuality.YTPlaybackQualityUnknownQuality.rawValue:
                            delegate.onPlaybackQualityChange(webView, quality: .YTPlaybackQualityUnknownQuality)
                        case PlayerQuality.kYTPlaybackQualityLargeQuality.rawValue:
                            delegate.onPlaybackQualityChange(webView, quality: .kYTPlaybackQualityLargeQuality)
                        case PlayerQuality.kYTPlaybackQualitySmallQuality.rawValue:
                            delegate.onPlaybackQualityChange(webView, quality: .kYTPlaybackQualitySmallQuality)
                        default:
                            break
                        }
                    case PlayerCallback.onPlayerError.rawValue:
                        switch data {
                        case PlayerError.YTPlayerErrorCannotFindVideoError.rawValue:
                            delegate.onPlayerError(webView, error: .YTPlayerErrorCannotFindVideoError)
                        case PlayerError.YTPlayerErrorHTML5Error.rawValue:
                            delegate.onPlayerError(webView, error: .YTPlayerErrorHTML5Error)
                        case PlayerError.YTPlayerErrorInvalidParamError.rawValue:
                            delegate.onPlayerError(webView, error: .YTPlayerErrorInvalidParamError)
                        case PlayerError.YTPlayerErrorNotEmbeddableError.rawValue:
                            delegate.onPlayerError(webView, error: .YTPlayerErrorNotEmbeddableError)
                        case PlayerError.YTPlayerErrorVideoNotFoundError.rawValue:
                            delegate.onPlayerError(webView, error: .YTPlayerErrorVideoNotFoundError)
                        case PlayerError.YTPlayerErrorSameAsNotEmbeddableError.rawValue:
                            delegate.onPlayerError(webView, error: .YTPlayerErrorSameAsNotEmbeddableError)
                        default:
                            break
                        }
                    default:
                        break
                    }
                }
            }
            
            return false
        }
        
        return true
    }

    open func webViewDidStartLoad(_ webView: UIWebView) {
        delegate.didStartLoad(webView)
    }
    
    open func webViewDidFinishLoad(_ webView: UIWebView) {
        delegate.didFinishLoad(webView)
    }
    
    open func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        delegate.didFailLoad(webView, withError: error as NSError?)
    }
}

