//
//  UIViewCotroller-MMDrawerController.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 10/20/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit
import MMDrawerController
    
public enum OpenDrawerGestureMode {
    case all
    case none
}
    
extension UIViewController {
    
    open func openDrawerGesture(mode: OpenDrawerGestureMode) {
        switch mode {
        case .all:
            setOpenDrawerGestureModeMaskAll()
        case .none:
            setOpenDrawerGestureModeMaskNone()
        }
    }
    
    open func setOpenDrawerGestureModeMaskAll() {
        if let drawer = mm_drawerController {
            drawer.openDrawerGestureModeMask = MMOpenDrawerGestureMode.all;
            drawer.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.all;
        }
    }
    
    open func setOpenDrawerGestureModeMaskNone() {
        if let drawer = mm_drawerController {
            drawer.openDrawerGestureModeMask = MMOpenDrawerGestureMode();
            drawer.closeDrawerGestureModeMask = MMCloseDrawerGestureMode();
        }
    }

    @objc open func onDrawerLeftMenuButton() {
        if let drawer = mm_drawerController {
            drawer.toggle(MMDrawerSide.left, animated:true, completion:nil)
        }
    }
    
    @objc open func onDrawerRightMenuButton() {
        if let drawer = mm_drawerController {
            drawer.toggle(MMDrawerSide.right, animated:true, completion:nil)
        }
    }
    
    open func onDrawerLeftMenuButtonWith(animated: Bool = true, completion: ((Bool) -> Void)? = nil) {
        if let drawer = mm_drawerController {
            drawer.toggle(MMDrawerSide.left, animated:animated, completion:completion)
        }
    }
    
    open func onDrawerRightMenuButtonWith(animated: Bool = true, completion: ((Bool) -> Void)? = nil) {
        if let drawer = mm_drawerController {
            drawer.toggle(MMDrawerSide.right, animated:animated, completion:completion)
        }
    }
}
