//
//  BaseViewController-MMDrawerController.swift
//  Alamofire
//
//  Created by Juan Pablo Crespi on 08/03/2018.
//

import Foundation
import UIKit
import MMDrawerController

extension BaseViewController {
    
    @IBAction open func onLeftMenuButtonTapped(_ sender: AnyObject) {
        onDrawerLeftMenuButton()
    }
    
    @IBAction open func onRightMenuButtonTapped(_ sender: AnyObject) {
        onDrawerRightMenuButton()
    }
}
