//
//  UINavigationItem-MMDrawerController.swift
//  Alamofire
//
//  Created by Juan Pablo Crespi on 08/03/2018.
//

import Foundation
import UIKit
import MMDrawerController

public extension UINavigationItem {
    
    func setLeftMenuButton(viewController vc: UIViewController, color: UIColor? = .white) {
        leftBarButtonItem = UIBarButtonItem(
            image: UIImage(named:navigationItemIconLeft),
            style: UIBarButtonItem.Style.plain,
            target: vc,
            action: #selector(vc.onDrawerLeftMenuButton))
        leftBarButtonItem?.tintColor = color
    }
    
    func setRightMenuButton(viewController vc: UIViewController, color: UIColor? = .white) {
        rightBarButtonItem = UIBarButtonItem(
            image: UIImage(named:navigationItemIconRight),
            style: UIBarButtonItem.Style.plain,
            target: vc,
            action: #selector(vc.onDrawerRightMenuButton))
        rightBarButtonItem?.tintColor = color
    }
    
}
