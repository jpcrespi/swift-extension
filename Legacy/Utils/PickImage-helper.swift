//
//  Camera.swift
//  pizzahut
//
//  Created by Gonzalo Sanchez on 3/5/17.
//  Copyright © 2017 Gonzalo Sanchez. All rights reserved.
//

import UIKit

public protocol ImagePickerDelegate {
    func presentCamaraViewController(_ viewController: UIViewController)
    func imagePickerControllerDidCancel()
    func imagePickerController(_ image: UIImage)
}

open class ImagePicker: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    public let picker = UIImagePickerController()
    open var delegate: ImagePickerDelegate?
    
    open func openGallary(_ controller: ImagePickerDelegate,
                     isTranslucent: Bool,
                     barTintColor: UIColor,
                     tintColor: UIColor,
                     titleTextColor: UIColor) {
        delegate = controller
        picker.delegate = self
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.navigationBar.isTranslucent = isTranslucent
        picker.navigationBar.barTintColor = barTintColor
        picker.navigationBar.tintColor = tintColor
        picker.navigationBar.titleTextAttributes = [.foregroundColor : titleTextColor]
        delegate?.presentCamaraViewController(picker)
    }

    open func openCamera(_ controller: ImagePickerDelegate,
                    isTranslucent: Bool,
                    barTintColor: UIColor,
                    tintColor: UIColor,
                    titleTextColor: UIColor) {
        
        delegate = controller
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            picker.delegate = self
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            picker.navigationBar.isTranslucent = isTranslucent
            picker.navigationBar.barTintColor = barTintColor
            picker.navigationBar.tintColor = tintColor
            picker.navigationBar.titleTextAttributes = [.foregroundColor : titleTextColor]
            delegate?.presentCamaraViewController(picker)
        } else {
            let alert = UIAlertController(
                title: "Camera Not Found",
                message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            delegate?.presentCamaraViewController(alert)
        }
    }
    
    open func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: {
            self.delegate?.imagePickerControllerDidCancel()
        })
    }
    
    open func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: {
            if let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                self.delegate?.imagePickerController(chosenImage)
            }
        })
    }
    
}

extension ImagePicker {
    
    open func showImagePicker(cameraTitle: String,
                         galleryTitle: String,
                         cancelTitle: String,
                         isTranslucent: Bool,
                         barTintColor: UIColor,
                         tintColor: UIColor,
                         titleTextColor: UIColor,
                         alertStyle: UIAlertController.Style,
                         presenterVC: UIViewController,
                         delegate: UIImagePickerControllerDelegate & UINavigationControllerDelegate) {
        
        func show(_ type: UIImagePickerController.SourceType) {
            picker.delegate = delegate
            picker.allowsEditing = false
            picker.sourceType = type
            picker.navigationBar.isTranslucent = isTranslucent
            picker.navigationBar.barTintColor = barTintColor
            picker.navigationBar.tintColor = tintColor
            picker.navigationBar.titleTextAttributes = [.foregroundColor : titleTextColor]
            presenterVC.present(picker, animated: true, completion: nil)
        }
        
        let alertController = UIAlertController(
            title: nil,
            message: nil,
            preferredStyle: alertStyle)
        
        alertController.addAction(UIAlertAction(title: cameraTitle, style: .default, handler: { (action) in
            show(.camera)
        }))
        
        alertController.addAction(UIAlertAction(title: galleryTitle, style: .default, handler: { (action) in
            show(.photoLibrary)
        }))
        
        alertController.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: { (action) in
        }))
        
        presenterVC.present(alertController, animated: true, completion: { })
    }
}
