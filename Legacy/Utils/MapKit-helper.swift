//
//  MapKit-helper.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 8/2/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import MapKit

open class MapKitHelper: NSObject {
    
    public static func openAppleMapsOnLocation(
        _ lat : Double,
        lon : Double,
        name : String,
        regionDistance: Double = 1000) {
        let coordinates = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "\(name)"
        mapItem.openInMaps(launchOptions: options)
    }
    
    public static func openGoogleMapsOnLocation(_ lat: Double, lon: Double, melat: Double, melon: Double) -> Bool {
        let googlemaps = "comgooglemaps://"
        
        if UIApplication.shared.canOpenURL(URL(string:googlemaps)!) {
            let saddr = "saddr=\(melat),\(melon)"
            let daddr = "daddr=\(lat),\(lon)"
            let directionsmode = "directionsmode=driving"
            if let url = URL(string:"\(googlemaps)?\(saddr)&\(daddr)&\(directionsmode)") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                return true
            }
        }
        return false
    }
    
    public static func openUber() {
        let deeplink = "uber://"
        let store = "itms-apps://itunes.apple.com/us/app/id368677368"
        
        if UIApplication.shared.canOpenURL(URL(string: deeplink)!) {
            if let url = URL(string:"\(deeplink)") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        } else {
            UIApplication.shared.open(URL(string: store)!, options: [:], completionHandler: nil)
        }
    }
    
    public static func openWaze(_ lat: Double, lon: Double) {
        let deeplink = "waze://"
        let store = "itms-apps://itunes.apple.com/us/app/id323229106"
        
        if UIApplication.shared.canOpenURL(URL(string: deeplink)!) {
            let ll = "ll=\(lat),\(lon)"
            let navigate = "navigate=yes"
            if let url = URL(string:"\(deeplink)?\(ll)&\(navigate)") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        } else {
            UIApplication.shared.open(URL(string: store)!, options: [:], completionHandler: nil)
        }
    }
    
    public static func centerMapOnLocation(
        _ mapView: MKMapView,
        location: CLLocation,
        regionRadious: CLLocationDistance) {
        
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadious * 2.0, longitudinalMeters: regionRadious * 2.0)
        UIView.animate(withDuration: 0.25, animations:
            { () -> Void in},
                       completion:
            {(finish) -> Void in
                mapView.setRegion(coordinateRegion, animated: true)
        })
    }
}

open class LocationAnnotation: NSObject, MKAnnotation {
    
    public let coordinate: CLLocationCoordinate2D
    public let title: String?
    public let distanceFromUserLocation: String?
    public let locationName: String
    
    init(title: String, locationName: String, coordinate: CLLocationCoordinate2D, distanceFromUserLocation: String?) {
        self.title = title
        self.locationName = locationName
        self.coordinate = coordinate
        self.distanceFromUserLocation = distanceFromUserLocation
        
        super.init()
    }
    
    open var subtitle: String? {
        return locationName
    }
}
