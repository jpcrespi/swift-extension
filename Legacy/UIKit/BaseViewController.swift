//
//  BaseViewController.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 17/12/15.
//  Copyright © 2015 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

public var kStatusHeight: CGFloat {
    return UIApplication.shared.statusBarFrame.height
}

public var kNavigationBarHeight: CGFloat {
    return 44
}

public var kTopLayoutHeight: CGFloat {
    return kStatusHeight + kNavigationBarHeight
}

open class BaseViewController: UIViewController, UIGestureRecognizerDelegate {
    
    open var deeplinkID: String? = nil
    open var interactivePopGestureRecognizer: Bool = false
    open var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0) // For dismiss pan gesture
    
    override open func viewDidLoad() {
        super.viewDidLoad()
    }

    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addNotificationKeyboard()
    }
    
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeNotificationKeyboard()
    }
    
    @IBAction open func navigationControllerDismissTapped(_ sender: AnyObject) {
        dismissNavigationController()
    }
    
    @IBAction open func navigationControllerPopTapped(_ sender: AnyObject) {
        popNavigationController()
    }
    
    open func interactivePopGestureRecognizer(_ enable: Bool) {
        interactivePopGestureRecognizer = enable
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    open func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return interactivePopGestureRecognizer
    }
    
    //MARK - Keyboard
    open func addNotificationKeyboard() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShowNotification(_:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil);
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHideNotification(_:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil);
    }
    
    open func removeNotificationKeyboard() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc open func keyboardWillShowNotification(_ sender: Foundation.Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            if let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject? {
                keyboardWillShow(keyboardFrame.cgRectValue.size.height)
            }
        }
    }
    
    @objc open func keyboardWillHideNotification(_ sender: Foundation.Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            if let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject? {
                keyboardWillHide(keyboardFrame.cgRectValue.size.height)
            }
        }
    }
    
    open func keyboardWillShow(_ keyboardHeight: CGFloat) {
        //self.view.frame.origin.y = 0
        //self.view.frame.origin.y -= keyboardHeight/2
        //self.view.layoutIfNeeded()
    }
    
    open func keyboardWillHide(_ keyboardHeight: CGFloat) {
        //self.view.frame.origin.y = 0
        //self.view.layoutIfNeeded()
    }
    
    open func addDismissPanGesture() {
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(panGesture))
        self.view.addGestureRecognizer(gesture)
    }
    
    @objc open func panGesture(_ sender:UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        if sender.state == UIGestureRecognizer.State.began {
            initialTouchPoint = touchPoint
        } else if sender.state == UIGestureRecognizer.State.changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == UIGestureRecognizer.State.ended || sender.state == UIGestureRecognizer.State.cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                dismissNavigationController()
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
}
