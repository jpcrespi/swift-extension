//
//  File.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 9/20/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

@available(iOS 8.0, *)
open class CustomBlurImageView: UIImageView {
    
    var blurView: APCustomBlurView?
    var opacityView: UIView?
    
    open func addBlur(withRadius radius: CGFloat? = nil) {
        if blurView == nil {
            if let blurRadius = radius {
                blurView = APCustomBlurView(withRadius: blurRadius)
            }
            else {
                blurView = APCustomBlurView()
            }
            
            blurView?.frame = frame
            blurView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            addSubview(blurView!)
        }
    }

    open func startBlur(withRadius radius: CGFloat, duration: TimeInterval) {
        UIView.animate(withDuration: duration) {
            self.blurView?.setBlurRadius(radius: radius)
        }
    }
}

@available(iOS 8.0, *)
open class APCustomBlurView: UIVisualEffectView {
    
    private let blurEffect: UIBlurEffect
    open var blurRadius: CGFloat {
        return blurEffect.value(forKeyPath: "blurRadius") as! CGFloat
    }
    
    public convenience init() {
        self.init(withRadius: 0)
    }
    
    public init(withRadius radius: CGFloat) {
        let customBlurClass: AnyObject.Type = NSClassFromString("_UICustomBlurEffect")!
        let customBlurObject: NSObject.Type = customBlurClass as! NSObject.Type
        self.blurEffect = customBlurObject.init() as! UIBlurEffect
        self.blurEffect.setValue(1.0, forKeyPath: "scale")
        self.blurEffect.setValue(radius, forKeyPath: "blurRadius")
        super.init(effect: radius == 0 ? nil : self.blurEffect)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func setBlurRadius(radius: CGFloat) {
        guard radius != blurRadius else {
            return
        }
        blurEffect.setValue(radius, forKeyPath: "blurRadius")
        self.effect = blurEffect
    }
}
