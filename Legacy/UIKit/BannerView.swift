//
//  LoadingView.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 8/2/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

open class BannerView: UIView {

    public enum BannerViewMode {
        case success
        case error
        case alert
    }
    
    public static var viewFont = UIFont.systemFont(ofSize: 15)

    public static var viewSuccess = "icon_success"
    public static var viewAlert = "icon_warning"
    public static var viewError = "icon_error"
    
    public static var viewHeight: CGFloat = 80
    public static var viewAnimation: TimeInterval = 0.3
    public static var viewDefaultTime: TimeInterval = 3
    
    public static var colorSuccess = UIColor(red: 0/255, green: 100/255, blue: 0/255, alpha: 1)
    public static var colorAlert = UIColor(red: 100/255, green: 100/255, blue: 0/255, alpha: 1)
    public static var colorError = UIColor(red: 100/255, green: 0/255, blue: 0/255, alpha: 1)

    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var centerView: UIControl!
    @IBOutlet weak var centerLabel: UILabel!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var leftIcon: UIImageView!
    
    public static var currentView: BannerView?
    
    open var customHeight = BannerView.viewHeight
    
    class open func show(withSuccess message: String, duration: TimeInterval = viewDefaultTime, height: CGFloat = viewHeight) {
        show(message, mode: .success, time: duration, height: height)
    }
    
    class open func show(withAlert message: String, duration: TimeInterval = viewDefaultTime, height: CGFloat = viewHeight) {
        show(message, mode: .alert, time: duration, height: height)
    }
    
    class open func show(withError message: String, duration: TimeInterval = viewDefaultTime, height: CGFloat = viewHeight) {
        show(message, mode: .error, time: duration, height: height)
    }
    
    class open func show(_ text: String, mode: BannerViewMode, time: TimeInterval, height: CGFloat) {
        if currentView == nil {
            let view: BannerView = initFromNib(bundle: Bundle(for: BannerView.self))
            let screen = UIApplication.shared.keyWindow?.frame ?? CGRect()
            let newFrame = CGRect(x: 0, y: screen.height-height, width: screen.width, height: height)
            
            view.customHeight = height
            view.frame = newFrame
            view.setup(text, mode: mode)
            currentView = view

            UIApplication.shared.keyWindow?.addSubview(view)
            Timer.scheduledTimer(timeInterval: time, target: self, selector: #selector(dismiss), userInfo: nil, repeats: false)
        }
    }
    
    @objc class open func dismiss() {
        currentView?.removeFromSuperview()
    }
    
    open func setup(_ text: String, mode: BannerViewMode) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = bounds
        gradient.colors = [UIColor.clear.cgColor, UIColor.lightGray.cgColor]
        gradientView.layer.insertSublayer(gradient, at: 0)
        
        gradientView.backgroundColor = UIColor.clear
        centerView.backgroundColor = UIColor.white
        centerLabel.font = BannerView.viewFont
        centerLabel.textColor = UIColor.gray
        centerLabel.text = text
        
        switch mode {
        case .alert:
            leftView.backgroundColor = BannerView.colorAlert
            leftIcon.setImageWithName(BannerView.viewAlert, color: UIColor.white)
        case .success:
            leftView.backgroundColor = BannerView.colorSuccess
            leftIcon.setImageWithName(BannerView.viewSuccess, color: UIColor.white)
        case .error:
            leftView.backgroundColor = BannerView.colorError
            leftIcon.setImageWithName(BannerView.viewError, color: UIColor.white)
        }
    }
    
    override open func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        
        gradientView.alpha = 0
        centerView.center = CGPoint(x: centerView.center.x, y: centerView.center.y + self.customHeight)
    }
    
    override open func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        UIView.animate(withDuration: BannerView.viewAnimation, animations: {
            self.gradientView.alpha = 1
            self.centerView.center = CGPoint(x: self.centerView.center.x, y: self.centerView.center.y - self.customHeight)
            }, completion: { (finish) in
        }) 
    }
    
    override open func removeFromSuperview() {
        UIView.animate(withDuration: BannerView.viewAnimation, animations: {
            self.gradientView.alpha = 0
            self.centerView.center = CGPoint(
                x: (self.centerView.center.x ),
                y: (self.centerView.center.y ) + self.customHeight)
        }, completion: { (finish) in
            super.removeFromSuperview()
            BannerView.currentView = nil
        }) 
    }
    
    @IBAction open func bannerTapped(_ sender: AnyObject) {
        removeFromSuperview()
    }
}
