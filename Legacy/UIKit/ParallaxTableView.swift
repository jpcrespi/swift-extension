//
//  ParallaxTableView.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 6/6/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

open class ParallaxTableView: UITableView {

    @IBOutlet open weak var headerView: UIView?
    @IBOutlet open weak var headerViewHeight: NSLayoutConstraint!
    
    open var kHeaderViewHeight: CGFloat!
    
    fileprivate var kHeaderFix: CGFloat = 0
    fileprivate var kBegin: CGFloat = 0
    fileprivate var kIgnore: CGFloat = 0
    
    open var scrollOffset:CGFloat = 0 {
        didSet {
            moveHeader()
        }
    }
    
    open func viewDidLoad(_ height : CGFloat, heightFix: CGFloat = 0, begin: CGFloat = 0, ignore: CGFloat = 0) {
        kHeaderFix = heightFix
        kBegin = begin
        kIgnore = ignore
        kHeaderViewHeight = height - begin
        headerViewHeight.constant = height - begin
        let frame = CGRect(x: 0, y: 0, width: bounds.width, height: height)
        headerView?.frame = frame
        tableHeaderView = headerView ?? UIView(frame: frame)
        tableHeaderView?.backgroundColor = headerView?.backgroundColor ?? UIColor.clear
        tableHeaderView?.isUserInteractionEnabled = headerView != nil
    }

    open func moveHeader() {
        if let hieght = kHeaderViewHeight {
            if scrollOffset + kHeaderFix < hieght {
                if scrollOffset < 0 {
                    if scrollOffset > -kIgnore {
                        headerViewHeight.constant = kHeaderViewHeight
                    }
                    else {
                        headerViewHeight.constant = kHeaderViewHeight - scrollOffset - kIgnore
                    }
                }
                else {
                    headerViewHeight.constant = kHeaderViewHeight - scrollOffset
                }
            }
            else {
                headerViewHeight.constant = kHeaderFix
            }
        }
    }
    
    open func viewDidLayoutSubviews() {
        moveHeader()
    }
    
    open func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollOffset = contentOffset.y
    }
}
