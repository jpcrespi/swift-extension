//
//  RealModelExtension.swift
//  Varon
//
//  Created by Juan Pablo Crespi on 23/10/2017.
//  Copyright © 2017 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import RealmSwift
import Realm.Private

open class RealModel {
    
    open class func setDefaultRealm(name: String,
                               inMemoryIdentifier: String? = nil,
                               syncConfiguration: SyncConfiguration? = nil,
                               encryptionKey: Data? = nil,
                               readOnly: Bool = false,
                               schemaVersion: UInt64 = 0,
                               migrationBlock: MigrationBlock? = nil,
                               deleteRealmIfMigrationNeeded: Bool = false,
                               shouldCompactOnLaunch: ((Int, Int) -> Bool)? = nil,
                               objectTypes: [Object.Type]? = nil) {
        Realm.Configuration.defaultConfiguration = Realm.Configuration(
            fileURL: URL(fileURLWithPath: RLMRealmPathForFile("\(name).realm"), isDirectory: false),
            inMemoryIdentifier: inMemoryIdentifier,
            syncConfiguration: syncConfiguration,
            encryptionKey: encryptionKey,
            readOnly: readOnly,
            schemaVersion: schemaVersion,
            migrationBlock: migrationBlock,
            deleteRealmIfMigrationNeeded: deleteRealmIfMigrationNeeded,
            shouldCompactOnLaunch: shouldCompactOnLaunch,
            objectTypes: objectTypes)
    }
}

extension String {
    
    public var array: [String] {
        get {
            return self.components(separatedBy: "|")
        }
        set {
            self = newValue.joined(separator: "|")
        }
    }
}

extension Array: CascadingDeletable {
    
    public func cascadingDeletions() -> [Any?] {
        return self
    }
}

extension Results: CascadingDeletable {
    
    public var array: [Element] {
        return Array(self)
    }
    
    public func cascadingDeletions() -> [Any?] {
        return array
    }
}

extension List: CascadingDeletable {
    
    public var array: [Element] {
        return Array(self)
    }
    
    public func cascadingDeletions() -> [Any?] {
        return array
    }
}

extension Realm {
    
    public static var instance: Realm {
        struct Static {
            static let instance: Realm = {
                if let realm = try? Realm() {
                    return realm
                }
                else {
                    fatalError()
                }
            }()
        }
        
        return Static.instance
    }
}

public protocol CascadingDeletable {
    
    func cascadingDeletions() -> [Any?]
}

public protocol DataSource  {
    associatedtype T: Object
    
    static func objects() -> [T]
    static func objects(withPredicate format: String, _ args: Any...) -> [T]
    static func objects(withTemplate template: String) -> [T]
    static func object<K>(byKey key: K) -> T?
    
    static func add(object: T?, update: Bool)
    static func add(objects: [T]?, update: Bool)
    
    static func delete()
    static func delete(withTemplate template: String)
    static func delete(object: T?)
    static func delete(objects: [T]?)
    static func delete<K>(byKey key: K)
    
    static func transaction(_ transaction: @escaping (() -> ()))
    static func transaction(_ transaction: @escaping (() -> ()), _ completion: (() -> ())?)
    
    static func update(list: List<T>, array: [T])
    
    static func log()
}

extension DataSource where T: CascadingDeletable {
    
    public static func objects() -> [T] {
        return Realm.instance.objects(T.self).array
    }
    
    public static func objects(withTemplate template: String) -> [T] {
        return Realm.instance.objects(T.self).filter("template = %@", template).array
    }

    public static func objects(withPredicate format: String, _ args: Any...) -> [T] {
        return Realm.instance.objects(T.self).filter(format, args).array
    }
    
    public static func object<K>(byKey key: K) -> T? {
        return Realm.instance.object(ofType: T.self, forPrimaryKey: key)
    }
    
    public static func add(object: T?, update: Bool) {
        if let nonOptional = object {
            if Realm.instance.isInWriteTransaction {
                Realm.instance.add(nonOptional, update: .all)
            }
        }
    }
    
    public static func add(objects: [T]?, update: Bool) {
        if let nonOptional = objects {
            if Realm.instance.isInWriteTransaction {
                Realm.instance.add(nonOptional, update: .all)
            }
        }
    }
    
    public static func delete() {
        if Realm.instance.isInWriteTransaction {
            delete(cascading: objects())
        }
    }
    
    public static func delete(withTemplate template: String) {
        if Realm.instance.isInWriteTransaction {
            delete(cascading: objects(withTemplate: template))
        }
    }
    
    public static func delete(object: T?) {
        if let nonOptional = object {
            if Realm.instance.isInWriteTransaction {
                delete(cascading: nonOptional)
            }
        }
    }
    
    public static func delete(objects: [T]?) {
        if let nonOptional = objects {
            if Realm.instance.isInWriteTransaction {
                delete(cascading: nonOptional)
            }
        }
    }
    
    public static func delete<K>(byKey key: K) {
        if let nonOptional = object(byKey: key) {
            if Realm.instance.isInWriteTransaction {
                delete(cascading: nonOptional)
            }
        }
    }

    public static func transaction(_ transaction: @escaping (() -> ())) {
        try? Realm.instance.write {
            transaction()
        }
    }
    
    public static func transaction(_ transaction: @escaping (() -> ()), _ completion: (() -> ())?) {
        try? Realm.instance.write {
            transaction()
            completion?()
        }
    }

    public static func update(list: List<T>, array: [T]) {
        if Realm.instance.isInWriteTransaction {
            delete(cascading: list)
            list.append(objectsIn: array)
            Realm.instance.add(array)
        }
        else {
            list.append(objectsIn: array)
        }
    }
    
    public static func autoIncrementID() -> Int {
        return (Realm.instance.objects(T.self).max(ofProperty: "id") ?? 0) + 1
    }
    
    public static func log() {
        NSLog("\(T.className()): \(objects().count)")
    }
    
    fileprivate static func delete(cascading: CascadingDeletable) {
        for child in cascading.cascadingDeletions() {
            if let object = child as? CascadingDeletable {
                delete(cascading: object)
            }
        }
        
        if let object = cascading as? Object {
            Realm.instance.delete(object)
        }
    }
}

