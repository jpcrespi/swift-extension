//
//  Loading-helper.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 8/2/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit
    
import SVProgressHUD

open class LoadingHelper: NSObject {
    
    open class func config(_ maskType: SVProgressHUDMaskType = .gradient) {
        SVProgressHUD.setDefaultMaskType(maskType)
    }
    
    open class func showLoadingView() {
        SVProgressHUD.show()
    }
    
    open class func showLoadingView(withInfo message: String) {
        SVProgressHUD.showInfo(withStatus: message)
    }
    
    open class func dismissLoadingView() {
        SVProgressHUD.dismiss()
    }
    
    open class func dismissLoadingView(withSuccess message: String) {
        SVProgressHUD.showSuccess(withStatus: message)
    }
    
    open class func dismissLoadingView(withError message: String) {
        SVProgressHUD.showError(withStatus: message)
    }
}
