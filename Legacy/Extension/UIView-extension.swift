//
//  UIView-extesion.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 8/2/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

public extension UIView {
    
    func loadFromNib<T: UIView>(bundle: Bundle = Bundle.main) -> T? {
        if self.subviews.count == 0 {
            let view = bundle.loadNibNamed(self.className, owner: nil, options: nil)!.first as! T
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }
        return self as? T
    }
    
    class func initFromNib<T: UIView>(bundle: Bundle = Bundle.main) -> T {
        return bundle.loadNibNamed(self.className, owner: nil, options: nil)!.first as! T
    }
    
    func layerGradient(colorTop color1: UIColor, colorBottom color2: UIColor) {
        let sublayer: CAGradientLayer = CAGradientLayer()
        
        sublayer.locations = [0.05, 0.95]
        sublayer.startPoint = CGPoint(x: 0, y: 0)
        sublayer.endPoint = CGPoint(x: 0, y: 1)
        sublayer.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
        sublayer.colors = [color1.cgColor, color2.cgColor]
        
        layer.insertSublayer(sublayer, at: 0)
    }
    
    var absoluteFrame: CGRect? {
        if let window = UIApplication.shared.delegate?.window {
            return convert(bounds, to: window)
        }
        return nil
    }
    
    func rounded(_ radious: CGFloat, withColor color: UIColor) {
        backgroundColor = color
        roundBorders(value: radious)
    }
    
    func rounded(corners: UIRectCorner, cornerRadii: CGSize) {
        let path = UIBezierPath(roundedRect: self.bounds,
                                byRoundingCorners: corners,
                                cornerRadii: cornerRadii)
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
    
    func bordered(_ border: CGFloat, lineColor color: UIColor) {
        self.layer.borderWidth = border
        self.layer.borderColor = color.cgColor
    }
    
    func addShadow(color: UIColor, radious: CGFloat = 10, opacity: Float = 1) {
        self.layer.masksToBounds = false
//        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = radious
        self.layer.shadowOpacity = opacity
    }
    
    func removeShadow() {
        self.layer.shadowColor = UIColor.clear.cgColor
        self.layer.cornerRadius = 0
        self.layer.shadowRadius = 0
        self.layer.shadowOpacity = 0
    }
    
    func roundBorders(value: CGFloat = 5) {
        self.layer.cornerRadius = value
    }
    
    func circle() {
        self.layer.cornerRadius = self.frame.height / 2
    }
    
    func underlined(color: UIColor = .lightGray, lineHeight: CGFloat = 1) {
        let underline = UIView(frame: CGRect(x: 0, y: frame.height + lineHeight,
                                             width: frame.width, height: lineHeight))
        underline.backgroundColor = color
        underline.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleTopMargin]
        addSubview(underline)
        layer.masksToBounds = false
    }
    
}
