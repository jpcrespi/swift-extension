//
//  DateFormatter-extension.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 10/6/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

public var DefaultLocaleIdentifier: String? = nil
//"en_US_POSIX"
public var DefaultTimeZoneAbbreviation: String? = nil
//"UTC"

public extension DateFormatter {
    
    class var defaultLocale: Locale? {
        guard let locale = DefaultLocaleIdentifier else { return nil }
        return Locale(identifier: locale)
    }
    
    class var defaultTimeZone: TimeZone? {
        guard let timeZone = DefaultTimeZoneAbbreviation else { return nil }
        return TimeZone(abbreviation: timeZone)
    }
    
    @available(*, deprecated, message: "initWithUTC is deprecated, use initialization")
    class func initWithUTC() -> DateFormatter { return initialization() }
    
    class func initialization() -> DateFormatter {
        let formatter = DateFormatter()
        if let timeZone = defaultTimeZone { formatter.timeZone = timeZone }
        if let locale = defaultLocale { formatter.locale = locale }
        return formatter
    }
    
}
