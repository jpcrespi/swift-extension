//
//  UIApplication-extension.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 10/21/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

public extension UIApplication {
    
    var rootViewController: UIViewController? {
        get {
            return delegate?.window??.rootViewController
        }
        set (newValue) {
            delegate?.window??.rootViewController = newValue
        }
    }
    
    func changeRoot(withViewController viewController: UIViewController, animated: Bool = true, completion: (() -> ())? = nil) {
        if animated {
            DispatchQueue.main.async {
                if let snapshot = self.delegate?.window??.snapshotView(afterScreenUpdates: true) {
                    viewController.view.addSubview(snapshot)
                    self.rootViewController = viewController
                    
                    UIView.animate(withDuration: 0.5, animations: {() in
                        snapshot.layer.opacity = 0
                        snapshot.layer.transform = CATransform3DMakeScale(2, 2, 2)
                    }, completion: { (value: Bool) in
                        snapshot.removeFromSuperview()
                        completion?()
                    })
                }
            }
        } else {
            rootViewController = viewController
            completion?()
        }
    }
    
}
