//
//  UITextField-extension.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 11/8/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

public var UITextFieldDateFormat = "EEEE, dd MMM yyyy"

public extension UITextField {
    
    var pickerView: UIPickerView? {
        return inputView as? UIPickerView
    }
    
    var datePicker: UIDatePicker? {
        return inputView as? UIDatePicker
    }
    
    func setPickerView(target: Any,
                       rightSelector: Selector?,
                       rightTitle: String?,
                       leftSelector: Selector? = nil,
                       leftTitle: String? = nil) {
        
        let picker = UIPickerView(frame: .zero)
        
        picker.backgroundColor = .white
        picker.showsSelectionIndicator = true
        picker.delegate = target as? UIPickerViewDelegate
        picker.dataSource = target as? UIPickerViewDataSource
        picker.reloadAllComponents()
        inputView = picker
        
        setToolbar(target: target,
                   rightSelector: rightSelector,
                   rightTitle: rightTitle,
                   leftSelector: leftSelector,
                   leftTitle: leftTitle)
    }
    
    func setDatePicker(
        target: Any,
        rightSelector: Selector?,
        rightTitle: String?,
        leftSelector: Selector? = nil,
        leftTitle: String? = nil,
        dateSelector: Selector?,
        date: Date = Date(),
        maximumDate: Int,
        minimumDate: Int) {
        
        let picker = UIDatePicker(frame: .zero)
        
        picker.backgroundColor = .white
        picker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            picker.preferredDatePickerStyle = .wheels
        }
        picker.timeZone = TimeZone(abbreviation: DefaultTimeZoneAbbreviation ?? "")
        picker.locale = Locale(identifier: DefaultLocaleIdentifier ?? "")
        
        if let selector = dateSelector {
            picker.addTarget(target, action: selector, for: .valueChanged)
        } else {
            picker.addTarget(self, action: #selector(handleDatePicker(_:)), for: .valueChanged)
        }
        
        var gregorian: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        gregorian.timeZone = TimeZone(abbreviation: DefaultTimeZoneAbbreviation ?? "") ?? TimeZone.current
        let currentDate = Date()
        var components = DateComponents()
        
        components.year = -maximumDate
        picker.maximumDate = gregorian.date(byAdding: components, to: currentDate)
        
        components.year = -minimumDate
        picker.minimumDate = gregorian.date(byAdding: components, to: currentDate)

        picker.setDate(date, animated: false)
        if #available(iOS 14.0, *) {
            picker.preferredDatePickerStyle = .inline
        }
        
        inputView = picker

        setToolbar(target: target,
                   rightSelector: rightSelector,
                   rightTitle: rightTitle,
                   leftSelector: leftSelector,
                   leftTitle: leftTitle)
    }
    
    @objc func handleDatePicker(_ sender: UIDatePicker) {
        text = sender.date.dateFormat(UITextFieldDateFormat)
    }
    
    func setToolbar(target: Any,
                    rightSelector: Selector?,
                    rightTitle: String?,
                    leftSelector: Selector? = nil,
                    leftTitle: String? = nil) {
        
        inputAccessoryView = UITextField.getToolbar(target: target,
                                                    rightSelector: rightSelector,
                                                    rightTitle: rightTitle,
                                                    leftSelector: leftSelector,
                                                    leftTitle: leftTitle)
    }
    
    static func getToolbar(target: Any,
                           rightSelector: Selector?,
                           rightTitle: String?,
                           leftSelector: Selector? = nil,
                           leftTitle: String? = nil) -> UIToolbar {
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = .darkGray
        toolBar.sizeToFit()
        
        var rightButton: UIBarButtonItem
        var leftButton: UIBarButtonItem?
        
        if rightSelector == nil {
            rightButton = UIBarButtonItem(title: rightTitle, style: .plain, target: self, action: #selector(datePickerDone))
        } else {
            rightButton = UIBarButtonItem(title: rightTitle, style: .plain, target: target, action: rightSelector)
        }
        
        if let sel = leftSelector, let title = leftTitle {
            leftButton = UIBarButtonItem(title: title, style: .plain, target: target, action: sel)
        }
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        if let left = leftButton {
            toolBar.setItems([left, spaceButton, rightButton], animated: false)
        } else {
            toolBar.setItems([spaceButton, rightButton], animated: false)
        }
        
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
    
    @objc func datePickerDone() {
        self.resignFirstResponder()
    }
    
    func setPhoneNumber(string: String) -> Bool {
        let numbersOnly = string.numbersOnly
        
        var part1: String?
        if numbersOnly.count > 0 {
            part1 = "(\(numbersOnly.substring(from: 0, to: 2))"
        }
        
        var part2: String?
        if numbersOnly.count > 3 {
            part2 = ") \(numbersOnly.substring(from: 3, to: 5))"
        }
        
        var part3: String?
        if numbersOnly.count > 6 {
            part3 = " - \(numbersOnly.substring(from: 6, to: 9))"
        }
        
        text = "\(part1 ?? "")\(part2 ?? "")\(part3 ?? "")"
        
        return false
    }
    
    func setZipCode(string: String) -> Bool {
        let numbersOnly = string.numbersOnly
        
        var part1: String?
        if numbersOnly.count > 0 {
            part1 = "\(numbersOnly.substring(from: 0, to: 1))"
        }
        
        var part2: String?
        if numbersOnly.count > 2 {
            part2 = "\(numbersOnly.substring(from: 2, to: 4))"
        }
        
        text = "\(part1 ?? "")\(part2 ?? "")"
        
        return false
    }
    
    func set(font: UIFont, andColor color: UIColor) {
        self.font = font
        self.textColor = color
    }
    
}
