//
//  UIImage-extension.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 8/2/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

public extension UIImage {
    
    private func resizeData(maxWidth: CGFloat = 600, maxHeight: CGFloat = 600, compressionQuality: CGFloat = 1) -> Data? {
        var actualHeight : CGFloat = self.size.height
        var actualWidth : CGFloat = self.size.width
        var imgRatio : CGFloat = actualWidth / actualHeight
        let maxRatio : CGFloat = maxWidth / maxHeight
        
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if(imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            } else if(imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            } else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        
        let rect : CGRect = CGRect(x: 0, y: 0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        self.draw(in: rect)
        let imageData = UIImage().jpegData(compressionQuality: compressionQuality)
        UIGraphicsEndImageContext()
        
        return imageData
    }
    
    func resizeImage(maxWidth: CGFloat = 600, maxHeight: CGFloat = 600, compressionQuality: CGFloat = 1) -> UIImage? {
        if let data = resizeData(maxWidth: maxWidth, maxHeight: maxHeight, compressionQuality: compressionQuality) {
            return UIImage(data: data)
        }
        return nil
    }
    
    func with(color: UIColor) -> UIImage? {
        var image: UIImage? = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()
        image?.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
}
