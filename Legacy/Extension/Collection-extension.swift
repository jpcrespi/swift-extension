//
//  Collection-extension.swift
//  mbtb
//
//  Created by Juan Pablo Crespi on 5/17/17.
//  Copyright © 2017 Juan Pablo Crespi. All rights reserved.
//

import Foundation

public extension Collection {
    
    subscript (safe index: Index?) -> Iterator.Element? {
        guard let i = index, indices.contains(i) else { return nil }
        return self[i]
    }
    
}

public extension Array {
    
    mutating func rearrange(from indexFrom: Int?, to indexTo: Int?) {
        if let value = self[safe: indexFrom], let from = indexFrom, let to = indexTo {
            remove(at: from)
            insert(value, at: to)
        }
    }
    
}
