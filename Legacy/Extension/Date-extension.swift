//
//  Date-extension.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 10/6/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

public var BaseDateFormat = "EEEE, dd MMM yyyy HH:mm:ss Z"

public extension Date {
    
    func date000000() -> Date {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation:"UTC")!
        let components = (calendar as NSCalendar).components([.year, .month, .day], from: self)
        return calendar.date(from: components)!
    }
    
    func date235959() -> Date {
        return Date(timeIntervalSince1970: self.date000000().timeIntervalSince1970 + (60 * 60 * 24) - 1)
    }
    
    func dateFormat(_ dateFormat: String = BaseDateFormat) -> String {
        let formatter = DateFormatter.initialization()
        formatter.dateFormat = dateFormat
        return formatter.string(from: self)
    }
    
    func getDateFor(days: Int) -> Date {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation:"UTC")!
        let newDate = calendar.date(byAdding: .day, value: days, to: self)!
        let components = (calendar as NSCalendar).components([.year, .month, .day], from: newDate)
        return calendar.date(from: components)!
    }
    
}
