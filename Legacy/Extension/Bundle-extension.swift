//
//  Bundle-extension.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 10/7/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

public extension Bundle {
    
    static var bundleVersion: String? {
        return main.infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    static var bundleBuild: String? {
        return main.infoDictionary?["CFBundleVersion"] as? String
    }
    
}
