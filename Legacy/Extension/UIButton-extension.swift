//
//  UIButton-extesion.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 8/2/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

public extension UIButton {
    
    func setImage(_ name: String, color: UIColor) {
        setImage(UIImage(named: name), for: UIControl.State())
        tintColor = color
    }
    
    func setTitle(_ title: String, color: UIColor, font: UIFont) {
        setTitle(title, for: UIControl.State())
        setTitleColor(color, for: UIControl.State())
        titleLabel?.font = font
    }
    
}
