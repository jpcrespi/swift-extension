//
//  UIViewController-extension.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 8/2/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

    open class func initFrom<T>(_ storyboard: String) -> T? {
        return UIStoryboard(name: storyboard, bundle: nil).instantiateInitialViewController() as? T
    }
    
    // MARK: - Navigation
    @objc open func closeRightButton() {

    }
    
    @objc open func dismissNavigationController() {
        dismissNavigationControllerWith(animated: true, completion: nil)
    }
    
    open func dismissNavigationControllerWith(animated: Bool, completion: (() -> Void)?) {
        dismiss(animated: animated, completion: completion)
    }
    
    @objc open func popNavigationController() {
        popNavigationControllerWith(animated: true)
    }
    
    @objc open func popNavigationControllerWith(animated: Bool) {
        if let navigation = navigationController {
            if navigation.viewControllers.count > 1 {
                navigation.popViewController(animated: animated)
            }
            else {
                dismiss(animated: animated, completion: nil)
            }
        }
    }
    
    open func showAlert(
        title: String,
        message: String,
        preferredStyle: UIAlertController.Style,
        actions: (title: String, style: UIAlertAction.Style, handler: ((UIAlertAction) -> ())?)...) {
        
        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: preferredStyle)
        
        for action in actions {
            alertController.addAction(UIAlertAction(
                title: action.title,
                style: action.style,
                handler: action.handler))
        }
        
        present(alertController, animated: true) { }
    }
}

