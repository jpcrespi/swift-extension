//
//  CGFloat-extension.swift
//  m90
//
//  Created by Juan Pablo Crespi on 4/25/17.
//  Copyright © 2017 Juan Pablo Crespi. All rights reserved.
//

import UIKit

public extension CGFloat {
    
    init(randomInRange range: ClosedRange<CGFloat>) {
        let min = range.lowerBound
        let max = range.upperBound
        self = (CGFloat(arc4random()) / CGFloat(0xFFFFFFFF as UInt32)) * (max - min) + min
    }
    
}
