//
//  UIImageView-extension.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 10/22/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

public extension UIImageView {
    
    func setImageWithName(_ name: String) {
        image = UIImage(named: name)
    }
    
    func setImageWithName(_ name: String, color: UIColor?) {
        image = UIImage(named: name)?.withRenderingMode(.alwaysTemplate)
        tintColor = color
    }
    
}

