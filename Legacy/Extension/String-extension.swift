//
//  String-extension.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 8/2/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

public extension String {
    
    var number: NSNumber? {
        return NumberFormatter().number(from: self)
    }
    
    var numberValue: NSNumber {
        return number ?? NSNumber()
    }
    
}

public extension String {
    
    var numbersOnly: String {
        return components(separatedBy: CharacterSet(charactersIn: "1234567890").inverted).joined(separator: "")
    }
    
    var isNumbers: Bool {
        return !self.isEmpty && self.rangeOfCharacter(from: NSCharacterSet.decimalDigits.inverted) == nil
    }
    
    func DoubleToStringDecimalPlacesWithDouble(_ number:Double, numberOfDecimalPlaces:Int) -> String{
        return String(format:"%."+numberOfDecimalPlaces.description+"f", number)
    }
    
    func height(withWidth width: CGFloat = CGFloat.greatestFiniteMagnitude, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(
            with: constraintRect,
            options: NSStringDrawingOptions.usesLineFragmentOrigin,
            attributes: [.font: font],
            context: nil)
        
        return boundingBox.height
    }
    
    func width(withHeight height: CGFloat = CGFloat.greatestFiniteMagnitude, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(
            with: constraintRect,
            options: NSStringDrawingOptions.usesLineFragmentOrigin,
            attributes: [.font: font],
            context: nil)
        
        return boundingBox.width
    }
    
    func isEmpty() -> Bool {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""
    }
    
    func containsValidCharacters(regEx: String = "[A-Z0-9a-z. -]{0,}") -> Bool {
        let test = NSPredicate(format:"SELF MATCHES %@", regEx)
        return test.evaluate(with: self)
    }
    
    func isValidEmail() -> Bool {
        if self.count > 0 {
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailTest.evaluate(with: self)
        }
        return false
    }
    
    func isValidNumber() -> Bool {
        if self.count > 0 {
            let numbersRegEx = "[0-9]{1,}"
            let numbersTest = NSPredicate(format:"SELF MATCHES %@", numbersRegEx)
            return numbersTest.evaluate(with: self)
        }
        return false
    }
    
    func isOnlyText() -> Bool {
        if self.count > 0 {
            let textRegEx = "[A-Za-z ]{1,}" // Contains whitespace
            let textTest = NSPredicate(format:"SELF MATCHES %@", textRegEx)
            return textTest.evaluate(with: self)
        }
        return false
    }
    
    func isValidUsername() -> Bool {
        if self.count > 0 {
            let username = "[A-Za-z0-9.-_]{1,20}"
            let usernameTest = NSPredicate(format:"SELF MATCHES %@", username)
            return usernameTest.evaluate(with: self)
        }
        return false
    }
    
    func callPhone() -> Bool {
        if let phoneCallURL:URL = URL(string:"tel://"+"\(self.numbersOnly)"){
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
                return true
            }
        }
        return false
    }
    
    func openWhatsapp() -> Bool {
        guard let appURL = URL(string:"https://api.whatsapp.com/send?phone=\(self.numbersOnly)"), UIApplication.shared.canOpenURL(appURL) else {
            return false
        }
        UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
        return true
    }
    
    func attributedStringFromHTML(_ font: UIFont? = nil, color: UIColor? = nil) -> NSAttributedString? {
        if let data = self.data(using: String.Encoding.unicode) {
            if let attribute = try? NSMutableAttributedString(
                data: data,
                options: [.documentType: NSAttributedString.DocumentType.html],
                documentAttributes: nil) {
                if let fontName = font {
                    attribute.addAttributes(
                        [.font: fontName],
                        range: NSRange(location: 0, length: attribute.length))
                }
                if let colorName = color {
                    attribute.addAttributes(
                        [.foregroundColor: colorName],
                        range: NSRange(location: 0, length: attribute.length))
                }
                return attribute
            }
            
        }
        return nil
    }
    
    func substring(from: Int?, to: Int?) -> String {
        if let start = from {
            guard start < self.count else {
                return ""
            }
        }
        
        if let end = to {
            guard end > 0 else {
                return ""
            }
        }
        
        if let start = from, let end = to {
            if start == end {
                return self[start ..< end + 1]
            }
            guard end - start > 0 else {
                return ""
            }
        }
        
        let startIndex: String.Index
        if let start = from, start >= 0 {
            startIndex = self.index(self.startIndex, offsetBy: start)
        } else {
            startIndex = self.startIndex
        }
        
        let endIndex: String.Index
        if let end = to, end >= 0, end < self.count {
            endIndex = self.index(self.startIndex, offsetBy: end + 1)
        } else {
            endIndex = self.endIndex
        }
        
        return String(self[startIndex ..< endIndex])
    }
    
    subscript(i: Int) -> String {
        guard i >= 0 && i < count else { return "" }
        return String(self[index(startIndex, offsetBy: i)])
    }
    subscript(range: Range<Int>) -> String {
        let lowerIndex = index(startIndex, offsetBy: max(0,range.lowerBound), limitedBy: endIndex) ?? endIndex
        return substring(with: lowerIndex..<(index(lowerIndex, offsetBy: range.upperBound - range.lowerBound, limitedBy: endIndex) ?? endIndex))
    }
    subscript(range: ClosedRange<Int>) -> String {
        let lowerIndex = index(startIndex, offsetBy: max(0,range.lowerBound), limitedBy: endIndex) ?? endIndex
        return substring(with: lowerIndex..<(index(lowerIndex, offsetBy: range.upperBound - range.lowerBound + 1, limitedBy: endIndex) ?? endIndex))
    }
    
    func substring(from: Int) -> String {
        return self.substring(from: from, to: nil)
    }
    
    func substring(to: Int) -> String {
        return self.substring(from: nil, to: to)
    }
    
    func substring(from: Int?, length: Int) -> String {
        guard length > 0 else {
            return ""
        }
        
        let end: Int
        if let start = from, start > 0 {
            end = start + length - 1
        } else {
            end = length - 1
        }
        
        return self.substring(from: from, to: end)
    }
    
    func substring(length: Int, to: Int?) -> String {
        guard let end = to, end > 0, length > 0 else {
            return ""
        }
        
        let start: Int
        if let end = to, end - length > 0 {
            start = end - length + 1
        } else {
            start = 0
        }
        
        return self.substring(from: start, to: to)
    }
    
    func stringWithHTTP() -> String {
        if self.contains("http:") == false  &&
            self.contains("https:") == false &&
            self.count > 0 {
            return "\("http:")\(self)"
        }
        return self
    }
    
    func dateFormat(from fromFormat: String, to toFormat: String) -> String? {
        let formatter = DateFormatter.initialization()
        formatter.dateFormat = fromFormat
        guard let date = formatter.date(from: self) else { return nil }
        formatter.dateFormat = toFormat
        return formatter.string(from: date)
    }
    
    func dateFormat(format: String) -> Date? {
        let formatter = DateFormatter.initialization()
        formatter.dateFormat = format
        return formatter.date(from: self)
    }
    
    var predicate: NSPredicate {
        return NSPredicate(format: self)
    }
    
    func firstUppercased() -> String {
        guard let first = first else { return "" }
        return String(first).uppercased() + dropFirst()
    }
    
    func encodeUrl() -> String? {
        return self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
    }
    
    func decodeUrl() -> String? {
        return self.removingPercentEncoding
    }
    
}

public extension NSAttributedString {
    
    func height(withWidth width: CGFloat = CGFloat.greatestFiniteMagnitude) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(
            with: constraintRect,
            options: NSStringDrawingOptions.usesLineFragmentOrigin,
            context: nil)
        
        return boundingBox.height
    }
    
    func width(withHeight height: CGFloat = CGFloat.greatestFiniteMagnitude) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(
            with: constraintRect,
            options: NSStringDrawingOptions.usesLineFragmentOrigin,
            context: nil)
        
        return boundingBox.width
    }
    
}
