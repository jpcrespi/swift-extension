//
//  UINavigationItem-extension.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 8/2/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

public var navigationItemIconLeft = ""
public var navigationItemIconRight = ""
public var navigationItemIconClose = ""
public var navigationItemIconBack = ""
public var navigationItemIconDone = ""
public var navigationItemTitleFont = UIFont.systemFont(ofSize: 15)

public extension UINavigationItem {
    
    func setCenterTitle(_ title: String, color: UIColor? = .white) {
        let label = UILabel()
        label.frame = CGRect(origin: CGPoint.zero, size: CGSize(width: 44, height: 44))
        label.text = title
        label.textColor = color
        label.font = navigationItemTitleFont
        titleView = label
    }
    
    func setCenterLogo(viewController vc: UIViewController, named: String, height: CGFloat = 44, width: CGFloat = 176) {
        let view = UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: width, height: height)))
        let logo = UIImageView(image: UIImage(named: named))
        logo.frame = view.bounds
        logo.contentMode = UIView.ContentMode.scaleAspectFit
        view.addSubview(logo)
        titleView = view
    }
    
    func setCloseMenuButton(viewController vc: UIViewController, color: UIColor? = .white) {
        leftBarButtonItem = UIBarButtonItem(
            image: UIImage(named:navigationItemIconClose),
            style: UIBarButtonItem.Style.plain,
            target: vc,
            action: #selector(vc.dismissNavigationController))
        leftBarButtonItem?.tintColor = color
    }
    
    func setBackMenuButton(viewController vc: UIViewController, color: UIColor? = .white) {
        leftBarButtonItem = UIBarButtonItem(
            image: UIImage(named:navigationItemIconBack),
            style: UIBarButtonItem.Style.plain,
            target: vc,
            action: #selector(vc.popNavigationController))
        leftBarButtonItem?.tintColor = color
    }
    
    func setDoneMenuButton(viewController vc: UIViewController, color: UIColor? = .white) {
        leftBarButtonItem = UIBarButtonItem(
            image: UIImage(named:navigationItemIconDone),
            style: UIBarButtonItem.Style.plain,
            target: vc,
            action: #selector(vc.popNavigationController))
        leftBarButtonItem?.tintColor = color
    }
    
    func setCloseRightButton(viewController vc: UIViewController, color: UIColor? = .white) {
        rightBarButtonItem = UIBarButtonItem(
            image: UIImage(named:navigationItemIconClose),
            style: UIBarButtonItem.Style.plain,
            target: vc,
            action: #selector(vc.closeRightButton))
        rightBarButtonItem?.tintColor = color
    }
    
    public func setCustomRightButton(viewController vc: UIViewController, named: String, width: CGFloat = 44, height: CGFloat = 44, proportion: CGFloat = 1) {
        let view = UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: width, height: height)))
        let logo = UIImageView(frame: CGRect(x: width * (1 - proportion) / 2, y: height * (1 - proportion) / 2, width: width * proportion, height: height * proportion))
        logo.image = UIImage(named: named)
        logo.contentMode = .scaleAspectFit
        view.addSubview(logo)
        rightBarButtonItem = UIBarButtonItem(customView: view)
    }
}
