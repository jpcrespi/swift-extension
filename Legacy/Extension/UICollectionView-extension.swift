//
//  UICollectionView-extesion.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 9/22/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

public extension UICollectionView {
    
    func reloadData(_ completion: @escaping ()->()) {
        DispatchQueue.main.async(execute: {
            self.reloadData()
        })
        DispatchQueue.main.async(execute: {
            completion()
        });
    }
    
    func reusableCell<T>(withIdentifier identifier: T.Type, for indexPath: IndexPath) -> T {
        return dequeueReusableCell(withReuseIdentifier: String(describing: T.self), for: indexPath) as! T
    }
    
    func reusableSupplementaryView<T>(ofKind kind: String, withIdentifier identifier: T.Type, for indexPath: IndexPath) -> T {
        return dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: String(describing: T.self), for: indexPath) as! T
    }
    
}
