 //
//  Double-extension.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 9/28/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

public extension Double {
    
    var ms: TimeInterval           { return self / 1000 }
    var second: TimeInterval       { return self }
    var minute: TimeInterval       { return self * 60 }
    var hour: TimeInterval         { return self * 3600 }
    var day: TimeInterval          { return self * 3600 * 24 }
    
    func roundTo(places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    init(randomInRange range: ClosedRange<Double>) {
        let min = range.lowerBound
        let max = range.upperBound
        self = (Double(arc4random()) / Double(0xFFFFFFFF as UInt32)) * (max - min) + min
    }
    
}
