//
//  UITableView-extension.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 9/16/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit

public extension UITableView {
    
    func reloadDataWithAnimation(_ animation: UITableView.RowAnimation = .automatic) {
        let range = NSMakeRange(0, numberOfSections)
        let sections = IndexSet(integersIn: range.toRange() ?? 0..<0)
        reloadSections(sections, with: animation)
    }
    
    func reloadData(_ completion: @escaping () -> ()) {
        DispatchQueue.main.async(execute: {
            self.reloadData()
        })
        DispatchQueue.main.async(execute: {
            completion()
        });
    }
    
    func reusableCell<T>(withIdentifier identifier: T.Type) -> T {
        if let cell = dequeueReusableCell(withIdentifier: String(describing: T.self)) as? T  {
            return cell
        } else {
            fatalError("Error: dequeueReusableCell \(T.self)")
        }
    }
    
}
