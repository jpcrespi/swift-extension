//
//  Int-extension.swift
//  m90
//
//  Created by Juan Pablo Crespi on 4/24/17.
//  Copyright © 2017 Juan Pablo Crespi. All rights reserved.
//

import Foundation

public extension Int {
    
    init(randomInRange range: ClosedRange<Int>) {
        let min = range.lowerBound
        let max = range.upperBound
        self = Int(arc4random_uniform(UInt32(1 + max - min))) + min
    }
    
}
