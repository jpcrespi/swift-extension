//
//  AVPlayer-extension.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 23/11/2017.
//  Copyright © 2017 Juan Pablo Crespi. All rights reserved.
//

import AVKit

public extension AVPlayer {
    
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
    
}
