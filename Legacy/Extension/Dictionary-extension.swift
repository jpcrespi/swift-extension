//
//  Dictionary-extension.swift
//  mbtb
//
//  Created by Juan Pablo Crespi on 5/30/17.
//  Copyright © 2017 Juan Pablo Crespi. All rights reserved.
//

import Foundation

// MARK: - Plus Operator for Dictionary

/**
 Combine two dictionaries
 
 - parameter left:  left operand dictionary
 - parameter right: right operand dictionary
 
 - returns: Combined dictionary, existed keys in left dictionary will be overrided by right dictionary
 */
public func + <K, V> (left: [K : V], right: [K : V]?) -> [K : V] {
    guard let right = right else { return left }
    return left.reduce(right) {
        var new = $0 as [K : V]
        new.updateValue($1.1, forKey: $1.0)
        return new
    }
}

/**
 Combine two dictionaries
 
 - parameter left:  left operand dictionary
 - parameter right: right operand dictionary
 */
public func += <K, V> (left: inout [K : V], right: [K : V]){
    for (k, v) in right {
        left.updateValue(v, forKey: k)
    }
}

extension Dictionary {
    
    public var hashValue: Int {
        return createHashString(dict: self).hashValue
    }
    
    public static func == (lhs: Dictionary, rhs: Dictionary) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
    
    private func createHashString(dict: Dictionary, initialValue: String = "") -> String {
        var value: String = initialValue
        let sortedKeys = dict.keys.sorted(by: { $0.hashValue < $1.hashValue })
        for key in sortedKeys {
            if let dictValue = dict[key] {
                if dictValue is Dictionary {
                    value += createHashString(dict: dictValue as! Dictionary, initialValue: value)
                } else {
                    var hash: Int = 0
                    if dictValue is AnyHashable {
                        hash = (dictValue as! AnyHashable).hashValue
                    }
                    value += "." + String( (String(key.hashValue) + ":" + String(hash)).hashValue )
                }
            }
        }
        return value
    }
}
