//
//  UIImageView-Kingfisher.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 10/20/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

public var IMAGE_STAGING_USER: String? = nil
public var IMAGE_STAGING_PASSWORD: String? = nil
    
public extension UIImageView {
    
    func setImageWithURL(
        _ urlString: String,
        placeholder: String? = nil,
        color: UIColor? = nil,
        completion: (() -> ())? = nil) {
        if let url = URL(string: getURL(urlString)) {
            
            var placeholderImage: UIImage?
            if let placeholderOptional = placeholder {
                placeholderImage = UIImage(named: placeholderOptional)
            }
            
            kf.setImage(
                with: url,
                placeholder: placeholderImage,
                options: nil,
                completionHandler: { (image, error, cacheType, imageURL) -> () in
                    if let imageOptional = image {
                        if let colorOptional = color {
                            self.image = imageOptional.withRenderingMode(.alwaysTemplate)
                            self.tintColor = colorOptional
                        } else {
                            self.image = imageOptional
                        }
                    }
                    completion?()
            })
        }
    }
    
    func getURL(_ urlString: String) -> String {
        if let user = IMAGE_STAGING_USER, let password = IMAGE_STAGING_PASSWORD {
            return urlString.replacingOccurrences(of: "http://", with: "http://\(user):\(password)@")
        } else {
            return urlString
        }
    }
    
}
