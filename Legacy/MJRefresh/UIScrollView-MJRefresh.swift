//
//  UIScrollView-MJRefresh.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 10/20/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import UIKit
import MJRefresh
    
public extension UIScrollView {

    func addRefreshControl(_ handler: @escaping (() -> Void), insetTop: CGFloat = 0, style: UIActivityIndicatorView.Style = .gray) {
        let header = MJRefreshNormalHeader(refreshingBlock: handler)
        header.lastUpdatedTimeLabel?.isHidden = true
        header.stateLabel?.isHidden = true
        header.activityIndicatorViewStyle = style
        mj_header = header
        mj_header?.ignoredScrollViewContentInsetTop = insetTop
    }

    func removeRefreshControl() {
        mj_header?.removeFromSuperview()
        mj_header = nil
    }
    
    func isRefreshing() -> Bool {
        return mj_header?.state == .refreshing
    }
    
    func endRefreshing() {
        mj_header?.endRefreshing()
    }
    
}
