//
//  Localized.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 1/10/17.
//  Copyright © 2017 Juan Pablo Crespi. All rights reserved.
///Users/jpcrespi/Repositories/cmx-app-ios/Cmxapp

import UIKit
import Foundation

// Base bundle as fallback.
public let LCLBaseBundle = "Base"

// Current language
public let LCLCurrentLanguageKey = "LCLCurrentLanguageKey"

// Default language is English. Defaults to base localization if English is unavailable.
public let LCLDefaultLanguage = "en"

// Name for language change notification
public let LCLLanguageChangeNotification = "LCLLanguageChangeNotification"

// MARK: - Language Functions
public class Localized: NSObject {
    
    /// List of available languages, returns an Array of the available languages,
    /// If excludeBase = true, don't include "Base" in available languages.
    public class func availableLanguage(_ excludeBase: Bool = false) -> [String] {
        var availableLanguage = Bundle.main.localizations
        if let indexOfBase = availableLanguage.firstIndex(of: "Base"), excludeBase == true {
            availableLanguage.remove(at: indexOfBase)
        }
        return availableLanguage
    }
    
    /// Current language, returns the current language in String.
    public class func currentLanguage() -> String {
        if let currentLanguage = UserDefaults.standard.object(forKey: LCLCurrentLanguageKey) as? String {
            return currentLanguage
        }
        return defaultLanguage()
    }
    
    /// Change the current language, parameter language: Desired language.
    public class func setCurrentLanguage(_ language: String) {
        let selectedLanguage = availableLanguage().contains(language) ? language : defaultLanguage()
        if (selectedLanguage != currentLanguage()){
            UserDefaults.standard.set(selectedLanguage, forKey: LCLCurrentLanguageKey)
            UserDefaults.standard.synchronize()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
        }
    }
    
    /// Returns: The app's default language. String.
    public class func defaultLanguage() -> String {
        var defaultLanguage: String = String()
        guard let preferredLanguage = Bundle.main.preferredLocalizations.first else {
            return LCLDefaultLanguage
        }
        let availableLanguages: [String] = self.availableLanguage()
        if (availableLanguages.contains(preferredLanguage)) {
            defaultLanguage = preferredLanguage
        } else {
            defaultLanguage = LCLDefaultLanguage
        }
        return defaultLanguage
    }
    
    /// Resets the current language to the default
    public class func resetCurrentLanguageToDefault() {
        setCurrentLanguage(self.defaultLanguage())
    }
    
    /// Returns a localized string for a specified language code.
    /// For example, in the "en" locale, the result for `"es"` is `"Spanish"`
    public class func displayNameForLanguage(language: String) -> String {
        let locale : Locale = Locale(identifier: currentLanguage())
        if let displayName = locale.localizedString(forLanguageCode: language) {
            return displayName
        }
        return String()
    }
}

// MARK: - Extension
public extension String {
    
    /// Return the localized string, replaces NSLocalizedString
    var localized: String {
        if let path = Bundle.main.path(forResource: Localized.currentLanguage(), ofType: "lproj"), let bundle = Bundle(path: path) {
            return bundle.localizedString(forKey: self, value: nil, table: nil)
        } else if let path = Bundle.main.path(forResource: LCLBaseBundle, ofType: "lproj"), let bundle = Bundle(path: path) {
            return bundle.localizedString(forKey: self, value: nil, table: nil)
        }
        return self
    }
    
    /// Returns: The formatted localized string with arguments, replaces String(format:NSLocalizedString)
    func localizedFormat(arguments: CVarArg...) -> String {
        return String(format: localized, arguments: arguments)
    }
    
}
