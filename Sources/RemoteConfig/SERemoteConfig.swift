//
//  SERemoteConfig.swift
//  verdurapp
//
//  Created by Juan Pablo on 17/04/2020.
//  Copyright © 2020 Juan Pablo. All rights reserved.
//

import Foundation
#if canImport(FirebaseRemoteConfig)
import FirebaseRemoteConfig

public protocol SERemoteConfig {
    
    var rawValue: String { get }
}

extension SERemoteConfig {
    
    public static var config: RemoteConfig {
        return RemoteConfig.remoteConfig()
    }
    
    public static func log(type: String, message: String) {
        #if DEBUG
        NSLog("[REMOTECONFIG][\(type)] \(message)")
        #endif
    }
    
    public static func setDefaults(fromPlist plist: String = "Config-Info") {
        config.setDefaults(fromPlist: plist)
    }
    
    public static func fetch(_ completion: ((String?) -> Void)? = nil) {
        config.fetch(withExpirationDuration: 1) { status, error1 in
            if status == .success {
                config.activate { _, error2 in
                    DispatchQueue.main.async { completion?(error2?.localizedDescription) }
                }
            } else {
                DispatchQueue.main.async { completion?(error1?.localizedDescription) }
            }
        }
    }
    
    public var string: String {
        return Self.config[rawValue].stringValue ?? ""
    }
    
    public var number: NSNumber {
        return Self.config[rawValue].numberValue
    }
    
    public var data: Data {
        return Self.config[rawValue].dataValue
    }
    
    public var bool: Bool {
        return Self.config[rawValue].boolValue
    }
}
#endif
