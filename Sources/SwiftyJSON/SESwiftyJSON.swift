//
//  SESwiftyJSON.swift
//  mbtb
//
//  Created by Juan Pablo Crespi on 1/24/17.
//  Copyright © 2017 Juan Pablo Crespi. All rights reserved.
//

import Foundation
#if canImport(SwiftyJSON)
import SwiftyJSON

public protocol Parsable {
    func parse(json: JSON)
}

public extension JSON {
    
    //Non-optional [String : Any]
    var dictionaryObjectValue: [String : Any] {
        return self.dictionaryObject ?? [:]
    }    
}
#endif
