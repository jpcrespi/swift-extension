//
//  BaseRequest.swift
//  IronRevival
//
//  Created by Juan Pablo on 03/08/2020.
//  Copyright © 2020 Nimble.LA. All rights reserved.
//

import Foundation

// class {Name}Request: BaseRequest<{DataToSend}, {ModelToParse}, {ResponseType}> {
//
//    class Data: NSObject, Codable {
//        var {key}: {ModelToParse}?
//    }
//
//    override init(data: {ModelToSend}) {
//        super.init(name: {ServiceName},
//                   path: {ServicePath},
//                   method: {ServiceMethod},
//                   data: data)
//    }
// }

public protocol SENetworkProtocol: NSObject {
    func initService(name: String)
    func finishService(name: String)
}

open class SENetworkRequest<T, U: Codable, V>: SENetwork<T, U, V>.Request { }
