//
//  SEDictionary.swift
//
//  Created by Juan Pablo on 31/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation

public extension Dictionary {
    
    mutating func mergeKeeping(_ other: [Key : Value]) {
        self.merge(other) { (current, _) in current }
    }
    
    mutating func mergeReplacing(_ other: [Key : Value]) {
        self.merge(other) { (_, new) in new }
    }
}
