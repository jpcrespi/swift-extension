//
//  SEEncodable.swift
//
//  Created by Juan Pablo on 31/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation

public extension Encodable {
    
    func dictionary() throws -> [String: AnyObject] {
        let data = try JSONEncoder().encode(self)
        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
        guard let dict = json as? [String: AnyObject] else { throw NSError() }
        return dict
    }
}
