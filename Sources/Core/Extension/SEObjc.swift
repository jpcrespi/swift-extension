//
//  SEObjectiveC.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 11/15/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
import ObjectiveC

public func setAssociatedObject<T: Any>(
    _ object: Any,
    _ value: T?,
    _ associativeKey: UnsafeRawPointer,
    _ policy: objc_AssociationPolicy = .OBJC_ASSOCIATION_RETAIN_NONATOMIC) {
    objc_setAssociatedObject(object, associativeKey, value,  policy)
}

public func getAssociatedObject<T: Any>(
    _ object: Any,
    _ associativeKey: UnsafeRawPointer) -> T? {
    return objc_getAssociatedObject(object, associativeKey) as? T
}

public func error<T>(function: String = #function,
              file: String = #file,
              line: Int = #line,
              message: String = "") -> T {
    fatalError("[function] > \(function) [file] > \(file) [line] > \(line) [message] > \(message)")
}