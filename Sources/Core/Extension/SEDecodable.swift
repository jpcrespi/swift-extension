//
//  SEDecodable.swift
//
//  Created by Juan Pablo on 31/03/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import Foundation

public extension Decodable {
    
    init(dictionary: [String: Any]) throws {
        let data = try JSONSerialization.data(withJSONObject: dictionary, options: [])
        self = try JSONDecoder().decode(Self.self, from: data)
    }
}
