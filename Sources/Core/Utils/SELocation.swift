//
//  SELocation.swift
//
//  Created by Juan Pablo on 04/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

#if canImport(UIKit)
import UIKit
import CoreLocation

open class SELocation: NSObject {
    
    open class var instance : SELocation {
        struct Static {
            static let sharedManager = SELocation()
        }
        return Static.sharedManager
    }
    
    internal func log(type: String, message: String) {
        #if DEBUG
        NSLog("[LOCATION][\(type)] \(message)")
        #endif
    }
    
    private var manager: CLLocationManager
    
    open var location: SEObservable<CLLocation?>
    open var status: SEObservable<CLAuthorizationStatus>
    open var authorized: Bool {
        return status.value == .authorizedAlways || status.value == .authorizedWhenInUse
    }
    
    public override init() {
        manager = CLLocationManager()
        location = SEObservable(value: nil)
        status = SEObservable(value: CLLocationManager.authorizationStatus())
        super.init()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        manager.allowsBackgroundLocationUpdates = true
    }
    
    open func requestAuthorization(authorization: CLAuthorizationStatus) {
        #if arch(i386) || arch(x86_64)
        DispatchQueue.main.async { self.status.notify() }
        #else
        if self.status.value == .notDetermined {
            switch authorization {
            case .authorizedAlways:
                if manager.responds(to: #selector(CLLocationManager.requestAlwaysAuthorization)) {
                    manager.requestAlwaysAuthorization()
                } else if manager.responds(to: #selector(CLLocationManager.requestLocation)) {
                    manager.requestLocation()
                } else {
                    DispatchQueue.main.async { self.status.notify() }
                }
            case .authorizedWhenInUse:
                if manager.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)) {
                    manager.requestWhenInUseAuthorization()
                } else if manager.responds(to: #selector(CLLocationManager.requestLocation)) {
                    manager.requestLocation()
                } else {
                    DispatchQueue.main.async { self.status.notify() }
                }
            default:
                DispatchQueue.main.async { self.status.notify() }
            }
        } else {
            DispatchQueue.main.async { self.status.notify() }
        }
        #endif
    }
    
    open func startUpdatingLocation() {
        #if arch(i386) || arch(x86_64)
        log(type: "START", message: "SimulatorNotSupported")
        #else
        if authorized {
            #if os(iOS)
            manager.startUpdatingLocation()
            #elseif os(watchOS)
            manager.requestLocation()
            #endif
            log(type: "START", message: "UpdatingLocation")
        }
        #endif
    }
    
    open func stopUpdatingLocation() {
        #if arch(i386) || arch(x86_64)
        log(type: "STOP", message: "SimulatorNotSupported")
        #else
        if authorized {
            manager.stopUpdatingLocation()
            log(type: "STOP", message: "UpdatingLocation")
        }
        #endif
    }
}

extension SELocation: CLLocationManagerDelegate {
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways:
            log(type: "AUTHORIZED", message: "Always")
        case .authorizedWhenInUse:
            log(type: "AUTHORIZED", message: "WhenInUse")
        case .denied:
            log(type: "AUTHORIZED", message: "Denied")
        case .restricted:
            log(type: "AUTHORIZED", message: "Restricted")
        case .notDetermined:
            log(type: "AUTHORIZED", message: "NotDetermined")
        default:
            log(type: "AUTHORIZED", message: "Unknown")
        }
        if status != .notDetermined {
            DispatchQueue.main.async { self.status.value = status }
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        log(type: "GET", message:  "\(locations.first?.coordinate.latitude ?? 0.0), \(locations.first?.coordinate.longitude ?? 0.0)")
        DispatchQueue.main.async { self.location.value = locations.first }
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        log(type: "ERROR", message: error.localizedDescription)
    }
}
#endif
