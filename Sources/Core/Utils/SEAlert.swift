//
//  SEAlert.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 8/2/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
#if canImport(UIKit)
import UIKit

open class SEAlert: NSObject {
    
    open class func show(
        title: String,
        message: String,
        style: UIAlertController.Style = .alert,
        presentAnimated: Bool = true,
        presentCompletion: (() -> ())? = nil,
        buttons: (title: String, style: UIAlertAction.Style, handler: (() -> ())?)...) {
        
        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: style)
        
        for button in buttons {
            alertController.addAction(UIAlertAction(title: button.title, style: button.style, handler: { (action) in button.handler?() }))
        }
        
        DispatchQueue.main.async {
            let root = UIApplication.shared.keyWindow?.rootViewController
            (root?.presentedViewController ?? root)?.present(alertController, animated: presentAnimated, completion: presentCompletion)
        }
    }
}
#endif
