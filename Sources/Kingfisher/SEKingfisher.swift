//
//  UIImageView-Kingfisher.swift
//  Cmxapp
//
//  Created by Juan Pablo Crespi on 10/20/16.
//  Copyright © 2016 Juan Pablo Crespi. All rights reserved.
//

import Foundation
#if !os(macOS)
import UIKit
import Kingfisher

open class SEAnimatedImageView: AnimatedImageView { 
    
}

public extension UIImageView {
    
    func setImage(
        withURL urlString: String,
        placeholder: String? = nil,
        color: UIColor? = nil,
        completion: ((NSError?) -> ())? = nil,
        user: String? = nil,
        password: String? = nil) {
        
        var urlValue: String
        if let user = user, let password = password {
            urlValue = setAuthenticate(urlString: urlString, user: user, password: password)
        } else {
            urlValue = urlString
        }
        
        guard let url = URL(string: urlValue) else { return }

        var placeholderImage: UIImage?
        if let placeholderOptional = placeholder {
            placeholderImage = UIImage(named: placeholderOptional)
        }
        
        kf.setImage(with: url,
                    placeholder: placeholderImage,
                    options: nil,
                    progressBlock: nil) { result in
                        switch result {
                        case .success(let value):
                            if let color = color {
                                self.image = value.image.withRenderingMode(.alwaysTemplate)
                                self.tintColor = color
                            } else {
                                self.image = value.image
                            }
                            completion?(nil)
                        case .failure(let error):
                            self.image = nil
                            completion?(error as NSError)
                        }
        }
    }
    
    private func setAuthenticate(urlString: String, user: String, password: String) -> String {
        return urlString.replacingOccurrences(of: "://", with: "://\(user):\(password)@")
    }
}
#endif
