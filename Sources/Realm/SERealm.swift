//
//  SERealm.swift
//  Varon
//
//  Created by Juan Pablo Crespi on 23/10/2017.
//  Copyright © 2017 Juan Pablo Crespi. All rights reserved.
//

import Foundation
#if canImport(RealmSwift)
import RealmSwift
import Realm.Private

open class SERealm {
    
    open class func setDefaultRealm(name: String,
                               inMemoryIdentifier: String? = nil,
                               syncConfiguration: SyncConfiguration? = nil,
                               encryptionKey: Data? = nil,
                               readOnly: Bool = false,
                               schemaVersion: UInt64 = 0,
                               migrationBlock: MigrationBlock? = nil,
                               deleteRealmIfMigrationNeeded: Bool = false,
                               shouldCompactOnLaunch: ((Int, Int) -> Bool)? = nil,
                               objectTypes: [Object.Type]? = nil) {
        Realm.Configuration.defaultConfiguration = Realm.Configuration(
            fileURL: URL(fileURLWithPath: RLMRealmPathForFile("\(name).realm"), isDirectory: false),
            inMemoryIdentifier: inMemoryIdentifier,
            syncConfiguration: syncConfiguration,
            encryptionKey: encryptionKey,
            readOnly: readOnly,
            schemaVersion: schemaVersion,
            migrationBlock: migrationBlock,
            deleteRealmIfMigrationNeeded: deleteRealmIfMigrationNeeded,
            shouldCompactOnLaunch: shouldCompactOnLaunch,
            objectTypes: objectTypes)
    }
}

public protocol SERealmDeletable {
    
    func cascadingDeletions() -> [SERealmDeletable?]
}

extension String {
    
    public var array: [String] {
        get {
            return self.components(separatedBy: "|")
        }
        set {
            self = newValue.joined(separator: "|")
        }
    }
}

extension Array: SERealmDeletable {
    
    public func cascadingDeletions() -> [SERealmDeletable?] {
        return compactMap { $0 as? SERealmDeletable }
    }
}

extension Results: SERealmDeletable {
    
    public var array: [Element] {
        return Array(self)
    }
    
    public func cascadingDeletions() -> [SERealmDeletable?] {
        return array.compactMap { $0 as? SERealmDeletable }
    }
}

extension List: SERealmDeletable {
    
    public var array: [Element] {
        return Array(self)
    }
    
    public func cascadingDeletions() -> [SERealmDeletable?] {
        return array.compactMap { $0 as? SERealmDeletable }
    }
}

extension Realm {
    
    public static var instance: Realm {
        struct Static {
            static let instance: Realm = {
                do {
                    return try Realm()
                } catch {
                    fatalError(error.localizedDescription)
                }
            }()
        }
        
        return Static.instance
    }
}

public protocol SERealmSource  {
    associatedtype T: Object & SERealmDeletable
    
    static func objects() -> [T]
    static func objects(withPredicate format: String, _ args: Any...) -> [T]
    static func objects(withTemplate template: String) -> [T]
    static func object<K>(byKey key: K) -> T?
    
    static func add(object: T?, update: Bool) throws
    static func add(objects: [T]?, update: Bool) throws
    
    static func delete() throws
    static func delete(withTemplate template: String) throws
    static func delete(object: T?) throws
    static func delete(objects: [T]?) throws
    static func delete<K>(byKey key: K) throws
    
    static func transaction(_ block: @escaping (() throws -> Void))
    static func transaction(_ block: @escaping (() throws -> Void), competion: ((Error?) -> Void)?)
    
    static func update(list: List<T>, array: [T])
}

extension SERealmSource {
    
    public static func objects() -> [T] {
        return Realm.instance.objects(T.self).array
    }
    
    public static func objects(withTemplate template: String) -> [T] {
        return Realm.instance.objects(T.self).filter("template = %@", template).array
    }

    public static func objects(withPredicate format: String, _ args: Any...) -> [T] {
        return Realm.instance.objects(T.self).filter(format, args).array
    }
    
    public static func object<K>(byKey key: K) -> T? {
        return Realm.instance.object(ofType: T.self, forPrimaryKey: key)
    }
    
    public static func add(object: T?, update: Bool) throws {
        guard Realm.instance.isInWriteTransaction else { throw NSError() }
        guard let object = object else { throw NSError() }
        Realm.instance.add(object, update: update ? .all : .modified)
    }
    
    public static func add(objects: [T]?, update: Bool) throws {
        guard Realm.instance.isInWriteTransaction else { throw NSError() }
        guard let objects = objects else { throw NSError() }
        Realm.instance.add(objects, update: update ? .all : .modified)
    }
    
    public static func delete() throws {
        guard Realm.instance.isInWriteTransaction else { throw NSError() }
        delete(cascading: objects())
    }
    
    public static func delete(withTemplate template: String) throws {
        guard Realm.instance.isInWriteTransaction else { throw NSError() }
        delete(cascading: objects(withTemplate: template))
    }
    
    public static func delete(object: T?) throws {
        guard Realm.instance.isInWriteTransaction else { throw NSError() }
        guard let object = object else { throw NSError() }
        delete(cascading: object)
    }
    
    public static func delete(objects: [T]?) throws {
        guard Realm.instance.isInWriteTransaction else { throw NSError() }
        guard let objects = objects else { throw NSError() }
        delete(cascading: objects)
    }
    
    public static func delete<K>(byKey key: K) throws {
        guard Realm.instance.isInWriteTransaction else { throw NSError() }
        guard let object = object(byKey: key) else { throw NSError() }
        delete(cascading: object)
    }

    public static func transaction(_ block: @escaping (() throws -> Void)) {
        transaction(block, competion: nil)
    }
    
    public static func transaction(_ block: @escaping (() throws -> Void), competion: ((Error?) -> Void)?) {
        do {
            try Realm.instance.write {
                try block()
                competion?(nil)
            }
        } catch {
            competion?(error)
        }
    }

    public static func update(list: List<T>, array: [T]) {
        if Realm.instance.isInWriteTransaction {
            delete(cascading: list)
            list.append(objectsIn: array)
            Realm.instance.add(array)
        } else {
            list.append(objectsIn: array)
        }
    }
    
    public static func autoIncrementID() -> Int {
        return (Realm.instance.objects(T.self).max(ofProperty: "id") ?? 0) + 1
    }
    
    fileprivate static func delete(cascading: SERealmDeletable) {
        for child in cascading.cascadingDeletions() {
            if let child = child {
                delete(cascading: child)
            }
        }
        if let object = cascading as? Object {
            Realm.instance.delete(object)
        }
    }
}
#endif
