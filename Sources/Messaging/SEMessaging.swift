//
//  SEMessaging.swift
//
//  Created by Juan Pablo on 04/04/2020.
//  Copyright © 2020 eCloudSolutions. All rights reserved.
//

import UserNotifications
#if canImport(UIKit)
import UIKit
#if canImport(FirebaseMessaging)
import FirebaseMessaging
#endif

public enum SEMessagingDefault: String, SEDefaultDelegate {
    case token
    case firebase
}

open class SEMessaging: NSObject {
    
    internal func log(type: String, message: String) {
        #if DEBUG
        NSLog("[NOTIFICATIONS][\(type)] \(message)")
        #endif
    }
    
    open var options: UNAuthorizationOptions = [.alert, .badge, .sound]
    open var notification: SEObservable<[AnyHashable: Any]?>
    open var deviceToken: SEObservable<(Data?, NSError?)>
    open var firebaseToken: SEObservable<String?>
    open class var instance : SEMessaging {
        struct Static {
            static let sharedManager = SEMessaging()
        }
        return Static.sharedManager
    }
    
    public override init() {
        notification = SEObservable(value: nil)
        deviceToken = SEObservable(value: (try? SEMessagingDefault.token.get(), nil))
        firebaseToken = SEObservable(value: try? SEMessagingDefault.firebase.get())
        super.init()
        #if canImport(FirebaseMessaging)
        Messaging.messaging().delegate = self
        #endif
    }
    
    open func requestAuthorization() {
        if UIApplication.shared.isRegisteredForRemoteNotifications {
            DispatchQueue.main.async { self.deviceToken.notify() }
        } else {
            UNUserNotificationCenter.current().requestAuthorization(options: options) { granted, error in
                DispatchQueue.main.async {
                    if granted {
                        UIApplication.shared.registerForRemoteNotifications()
                    } else {
                        self.deviceToken.notify()
                    }
                }
            }
        }
    }

    open func did(registerForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        DispatchQueue.main.async { self.deviceToken.value = (deviceToken, nil) }
        let tokenString = deviceTokenToString(withData: deviceToken)
        log(type: "DeviceToken", message: tokenString)
        SEMessagingDefault.token.set(value: tokenString)
        #if canImport(FirebaseMessaging)
        Messaging.messaging().apnsToken = deviceToken
        did(receiveMessagingRegistrationToken: Messaging.messaging().fcmToken)
        #endif
    }

    open func did(receiveMessagingRegistrationToken firebaseToken: String?) {
        guard let firebaseToken = firebaseToken else { return }
        DispatchQueue.main.async { self.firebaseToken.value = firebaseToken }
        log(type: "FirebaseToken", message: firebaseToken)
        SEMessagingDefault.firebase.set(value: firebaseToken)
    }
    
    open func did(failToRegisterForRemoteNotificationsWithError error: Error) {
        DispatchQueue.main.async { self.deviceToken.value = (nil, error as NSError) }
        log(type: "DeviceToken", message: error.localizedDescription)
        SEMessagingDefault.token.remove()
    }
    
    open func did(receiveRemoteNotification userInfo: [AnyHashable: Any]) {
        notification.value = userInfo
    }
    
    private func deviceTokenToString(withData deviceToken: Data) -> String {
        let tokenChars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var tokenString = ""
        for index in 0..<deviceToken.count {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[index]])
        }
        return tokenString
    }
}
#if canImport(FirebaseMessaging)
extension SEMessaging: MessagingDelegate {

    public func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        did(receiveMessagingRegistrationToken: fcmToken)
    }
}
#endif
#endif
