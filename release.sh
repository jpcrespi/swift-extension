#!/bin/bash
set -eu

DIR=$(cd "$(dirname "$0")" && pwd -P)
cd $DIR

OLD_VERSION=$(defaults read $DIR/Sources/Info CFBundleShortVersionString)

echo "Current version: $OLD_VERSION"
read -p 'New version: ' NEW_VERSION

defaults write $DIR/Sources/Info CFBundleShortVersionString $NEW_VERSION

sed -i '' "s/$OLD_VERSION/$NEW_VERSION/" SwiftExtension.podspec
sed -i '' "s/$OLD_VERSION/$NEW_VERSION/" SwiftExtensionDEV.podspec

$EDITOR $DIR/CHANGELOG.md

#pod trunk push SwiftExtension.podspec

git add CHANGELOG.md Sources/Info.plist SwiftExtension.podspec SwiftExtensionDEV.podspec
git commit -m"Release $NEW_VERSION"
git tag $NEW_VERSION
git push
git push --tags