Pod::Spec.new do |s|

  s.name                  = "LegacyExtension"
  s.version               = "1.1.0"
  s.summary               = "Legacy Extension"
  s.description           = <<-DESC
                            Swift Extension by Juan Pablo Crespi
                           DESC
  s.homepage              = "https://gitlab.com/jpcrespi/swift-extension"
  s.license               = { :type => "Commercial", :text => "Copyright 2017" }
  s.author                = "Juan Pablo Crespi"

  s.platform        	    = :ios, "11.0"
  s.source          	    = { "path" => "." }
  s.requires_arc    	    = true
  s.default_subspec       = 'Core'

  s.subspec 'Core' do |core|
    core.dependency 'SwiftExtension/Legacy/Utils'
    core.dependency 'SwiftExtension/Legacy/UIKit'
    core.dependency 'SwiftExtension/Legacy/Extension'
  end

  s.subspec 'Utils' do |utils|
    utils.source_files = 'Legacy/Utils/**/*.*'
  end

  s.subspec 'Managers' do |managers|
    managers.source_files = 'Legacy/Managers/**/*.*'
    managers.dependency 'SwiftyJSON'
  end

  s.subspec 'UIKit' do |uikit|
    uikit.source_files = 'Legacy/UIKit/**/*.*'
  end

  s.subspec 'Extension' do |extension|
    extension.source_files = 'Legacy/Extension/**/*.*'
  end
    
  s.subspec 'MJRefresh' do |mjrefresh|
    mjrefresh.source_files = 'Legacy/MJRefresh/**/*.*'
    mjrefresh.dependency 'MJRefresh'
  end
    
  s.subspec 'SVProgressHUD' do |svprogresshud|
    svprogresshud.source_files = 'Legacy/SVProgressHUD/**/*.*'
    svprogresshud.dependency 'SVProgressHUD'
  end
    
  s.subspec 'MMDrawerController' do |mmdrawerController|
    mmdrawerController.source_files = 'Legacy/MMDrawerController/**/*.*'
    mmdrawerController.dependency 'MMDrawerController'
  end

  s.subspec 'Realm' do |realm|
    realm.source_files = 'Legacy/Realm/**/*.*'
    realm.dependency 'RealmSwift'
  end

  s.subspec 'Alamofire' do |alamofire|
    alamofire.source_files = 'Legacy/Alamofire/**/*.*'
    alamofire.dependency 'Alamofire', '4.9.1'
  end

  s.subspec 'SwiftyJSON' do |swiftyjson|
    swiftyjson.source_files = 'Legacy/SwiftyJSON/**/*.*'
    swiftyjson.dependency 'SwiftyJSON'
  end

  s.subspec 'Kingfisher' do |kingfisher|
    kingfisher.source_files = 'Legacy/Kingfisher/**/*.*'
    kingfisher.dependency 'Kingfisher', '5.10.1'
  end
end
