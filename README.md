# README

# SwiftExtension

## How to install

### [Swift Package Manager](https://www.swift.org/package-manager)

Once you have your Swift package set up, adding SwiftExtension as a dependency is as easy as adding it to the `dependencies` value of your `Package.swift`.

```swift
dependencies: [
    .package(url: "https://gitlab.com/jpcrespi/swift-extension.git", .upToNextMajor(from: "2.0.0"))
]
```

### [CocoaPods](http://cocoapods.org/)

To integrate Swift Extension into your Xcode project using CocoaPods, specify it in your `Podfile`:

```ruby
pod 'SwiftExtension', :git => 'https://gitlab.com/jpcrespi/swift-extension.git', :tag => '2.X.X'
```

Subspecs:

```ruby
pod 'SwiftExtension/Utils'
pod 'SwiftExtension/Extension'
pod 'SwiftExtension/Alamofire'
pod 'SwiftExtension/Kingfisher'
pod 'SwiftExtension/SwiftyJSON'
pod 'SwiftExtension/Realm'
pod 'SwiftExtension/RemoteConfig'
pod 'SwiftExtension/Messaging'
```

```ruby
pod 'LegacyExtension', :git => 'https://gitlab.com/jpcrespi/swift-extension.git', :tag => '2.X.X'
```

```ruby
pod 'LegacyExtension'
pod 'LegacyExtension/Utils'
pod 'LegacyExtension/UIKit'
pod 'LegacyExtension/Extension'
pod 'LegacyExtension/MJRefresh'
pod 'LegacyExtension/SVProgressHUD'
pod 'LegacyExtension/MMDrawerController'
```

Then, run the following command:

```bash
pod install --repo-update
```

# Author

- [Juan Pablo Crespi](https://gitlab.com/jpcrespi)
- [Gonzalo Sanchez](https://gitlab.com/gonzalo.sanchez)
