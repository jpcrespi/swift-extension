# SwiftExtension Changelog #

## 2.0.8

- Fix SENetwork thread issues

## 2.0.7

- Add CHANGELOG.md file
